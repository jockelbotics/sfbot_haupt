//Atmega644P - analog.h

#ifndef ANALOG_H
#define ANALOG_H

////////////////////////////////////////////////////////////
//
// Initialisierung der analogen Eing�nge
//
// Befehl: analog_init(); 
//
// Nur ein Beispiel
//
////////////////////////////////////////////////////////////

void analog_init(void) 
{
//    ADCSRA = (1 << ADEN) | (1 << ADPS0) | (1 << ADPS1) | (1 << ADPS2);
  uint16_t result;
 
  // externe Referenzspannung als Refernz f�r den ADC w�hlen:
  // ADMUX = (1<<REFS0);
  ADMUX = 0;
  //ADMUX |= (1<<5); //ADLAR BIT
  // Bit ADFR ("free running") in ADCSRA steht beim Einschalten
  // schon auf 0, also single conversion
  ADCSRA  = (1<<ADPS0) | (1<<ADPS1)| (1<<ADPS2);	// Frequenzvorteiler = 128
  ADCSRA |= (1<<ADEN);                  			// ADC aktivieren
 
  /* nach Aktivieren des ADC wird ein "Dummy-Readout" empfohlen, man liest
     also einen Wert und verwirft diesen, um den ADC "warmlaufen zu lassen" */
 
  ADCSRA |= (1<<ADSC);                  // eine ADC-Wandlung 
  while (ADCSRA & (1<<ADSC) ) {         // auf Abschluss der Konvertierung warten
  }
  /* ADCW muss einmal gelesen werden, sonst wird Ergebnis der n�chsten
     Wandlung nicht �bernommen. */
  result = ADCW;
}


////////////////////////////////////////////////////////////
//
// Abfrage der analogen Eing�nge 0-7
//
// Befehl   : y = analog(x); 
// Port     : x = {0;1;2;3;4;5;6;7}
// Ergebniss: Y = {0 - 1024}
//
////////////////////////////////////////////////////////////

uint16_t analog(uint8_t ch) 
{
	// Kanal waehlen, ohne andere Bits zu beeinflu�en
  	ADMUX = (ADMUX & ~(0x1F)) | (ch & 0x1F);
  	ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
    while (ADCSRA & (1<<ADSC));
    return (ADCL | (ADCH << 8));
}


////////////////////////////////////////////////////////////
//
// Abfrage der analogen Eing�nge 0-7 (Liefert den Mittelwert von 5 Messungen)
//
// Befehl   : y = analog5(x); 
// Port     : x = {2;3;4;5;6;7}
// Ergebniss: Y = {0 - 1024}
//
////////////////////////////////////////////////////////////

uint16_t analog5(uint8_t ch) 
{
    uint8_t 	i=0;
	uint32_t wert=0;
	while(i<5)
	{
		i++;
		ADMUX   = ch & 0x1f; 
    	ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
    	while (ADCSRA & (1<<ADSC));
		wert = wert + (ADCL | (ADCH << 8));
	}
    return (wert/i);
}

uint16_t analog8(uint8_t ch) 
{
    uint8_t 	i=0;
	uint32_t wert=0;
	while(i<8)
	{
		i++;
		ADMUX   = ch & 0x1f;
		ADMUX |= (1<<5); 
    	ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
    	while (ADCSRA & (1<<ADSC));
		wert = wert + (ADCH);
	}
    return ((wert)>>1);
}
////////////////////////////////////////////////////////////
//
// Abfrage der analogen Eing�nge 0-7 (Liefert den Mittelwert von 10 Messungen)
//
// Befehl   : y = analog10(x); 
// Port     : x = {0;1;2;3;4;5;6;7}
// Ergebniss: Y = {0 - 1024}
//
////////////////////////////////////////////////////////////

uint16_t analog10(uint8_t ch) 
{
    uint8_t 	i=0;
	uint32_t wert=0;
	while(i<10)
	{
		i++;
		ADMUX   = ch & 0x1f; 
    	ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
    	while (ADCSRA & (1<<ADSC));
		wert = wert + (ADCL | (ADCH << 8));
	}
    return (wert/i);
}

//////////////////////////////////
// Funktion zum auslesen der Sharpwerte und Umrechnung der Analogen Wert in echte L�ngen
// Auf der Hauptplatine befinden sich 6 Sharps 
// Die Nummerierung erfolgt von vorne im Uhrzeigersinn nach Rechts
//
//				0
//			5		1
//			
//			4		2
//				3
////////////////////////////////////

uint16_t sharp_abfragen(uint8_t nr)
{
	uint16_t rohwert=0;
	uint16_t returner=0;

	if(nr==0) //			NICHT ANGESCHLOSSEN (slot vorne)
	{
		//rohwert=analog5(7);
		//return rohwert;
		return 0;
	}
	else if(nr==1) //nach vorne Entfernung		(slot vorne rechts)
	{
		rohwert=9988/(analog5(5)-94);
		return rohwert;
	}
	else if(nr==2) //nach rechts Entfernung		(slot rechts)
	{
//		returner=((17493)/(analog10(4)+8));
//		returner = 9600/(analog5(4)+150);
		returner = 10416/(analog5(4)-45);
		return returner;
	}
	else if(nr==3) //nach hinten Entfernung		(slot hinten rechts)	
	{
//		returner=((5328)/(analog10(3)-55));
		returner = 2610 / (analog5(3)-28);
		return returner;
	}
	else if(nr==4) //	NICHT ANGESCHLOSSEN 	(linker slot)
	{
//		returner=(((17928)/(analog10(2)+13))-10);
		//returner = analog5(2);
		//return returner;
		return 0;
	}
	else if(nr==5) //nach links Entfernung		(slot vorne links)
	{
		rohwert= 10416 / (analog5(6)-45);
		return rohwert;
	}
	else return 0;
}

#endif
