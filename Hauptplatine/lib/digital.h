//Robo32 - digital.h



#ifndef DIDITAL_H
#define DIGITAL_H

#define MOSI 5
#define MISO 6
#define SCK	 7
#define SCHALTER1 (1<<0)
#define SCHALTER2 (1<<1)
#define SCHALTER3 (1<<2)
#define SCHALTER4 (1<<3)
#define TESTPIN	  2

// LEDs einschalten

void led_ein(int nr) // nr = {4;5}
{
    if(nr==4||nr==5) 
    {
        PORTD |= (1<<nr);
    }
}

// LEDs ausschalten

void led_aus(int nr) // nr = {4;5}
{
    if(nr==4||nr==5) 
    {
        PORTD  &= ~(1<<nr); 
    }
}

// LEDs tooglen

void led_toggle(int nr) // nr = {4;5}
{
    if(nr==4||nr==5) 
    {
        PORTD ^= (1<<nr);
    }
}

// Schalter abfragen

uint8_t schalter_abfragen(void)
{
	if ((PINC & (1<<3))) return 0;
	else return 1;

}

uint8_t schalter_neu(void)
{
	uint8_t sum = 0;
	
	if (PINB & (1<<SCK)) sum |= SCHALTER1;
	if (PINB & (1<<MOSI)) sum |= SCHALTER2;
	if (PINB & (1<<MISO)) sum |= SCHALTER3;
	if (PINC & (1<<TESTPIN)) sum |= SCHALTER4;
	
	return sum;

}

// IR abfragen

uint8_t ir_abfragen(uint8_t nr)
{
	if(nr==6)		// IR 6 vorne
	{
		if ((PIND & (1<<6))==0) return 1;
		else return 0;
	}
	else if(nr==7)	// IR 7 hinten
	{
		if ((PINB & (1<<4))==0) return 1;
		else return 0;
	}
	else return 2;
}


// Multiplexer (IC2)
//////////////////////////////
//  Diese Funktion schaltet den Multiplexer in die einzelnen Modi und fragt dann in diesen die Zust�nde der
//	Taster und der Dips ab und speichert diese Bitweise in ein Byte(returner).
//	Die Speicherung folgt in folgender Reihenfolge
//	Bit0: Taster4 Bit1: Taster3	Bit2-7: Dip0-5
///////////////////////////////

uint8_t multiplexer_abfragen(void)
{	
	uint8_t returner=0;

	//Taster0 wird Logik 1 wenn Leitung auf Low f�llt(0)
	PORTB  &= ~(0b00000111); 					// Modi umschalten
	_delay_us(2);			 					// kurze Pause um dem Multplexer Zeit zu geben um umzuschalten 2�s
	if ((PINB & (1<<3))==0)	returner |= (1<<0); // Bei gedr�cktem Taster oder Dip speichern des Wertes 

	// Analog zu Schritt eins jetzt die folgenden Modi ohne Kommentare

	//Dip0 wird Logik 1 wenn Leitung auf Low f�llt(0)
	PORTB  |= (0b00000001); 
	_delay_us(2);
	if ((PINB & (1<<3))==0)	returner |= (1<<2);

	//Dip1 
	PORTB  &= ~(0b00000111);
	PORTB  |=  (0b00000010);  
	_delay_us(2);
	if ((PINB & (1<<3))==0)	returner |= (1<<3);

	//Taster 3 
	PORTB  |=  (0b00000001);  
	_delay_us(2);
	if ((PINB & (1<<3))==0)	returner |= (1<<1);
	
	//Dip3
	PORTB  &= ~(0b00000111);
	PORTB  |=  (0b00000100);  
	_delay_us(2);
	if ((PINB & (1<<3))==0)	returner |= (1<<5);

	//Dip2 
	PORTB  |=  (0b00000001);  
	_delay_us(2);
	if ((PINB & (1<<3))==0)	returner |= (1<<4);

	//Dip4 
	PORTB  &= ~(0b00000111);
	PORTB  |=  (0b00000110);  
	_delay_us(2);
	if ((PINB & (1<<3))==0)	returner |= (1<<6);

	//Dip5 
	PORTB  |=  (0b00000001);  
	_delay_us(2);
	if ((PINB & (1<<3))==0)	returner |= (1<<7);

	return returner;
	
}

// Multiplexer (IC3)
//////////////////////////////
//  Diese Funktion schaltet den Multiplexer in die einzelnen Modi und fragt dann in diesen die Zust�nde der
//	Taster und der IRs ab und speichert diese Bitweise in ein Byte(returner).
//	Die Speicherung folgt in folgender Reihenfolge
//	Bit0: Taster1 Bit1: Taster2	Bit2-7: IR 0-5
///////////////////////////////

uint8_t multiplexer_ir_abfragen()
{
	uint8_t returner=0;

	//IR 5 hinten links
	PORTC  &= ~(0b01110000);
	//			   CBA
//	PORTC  |=  (0b01110000); 
	PORTC  |= (1<<6) | (1<<5) | (1<<4);  
	_delay_us(2);
	if ((PINC & (1<<7))==0)	returner |= (1<<7);

	//IR 3 links
	PORTC  &= ~(0b01110000);
	PORTC  |=  (0b01100000); 
	_delay_us(2);
	if ((PINC & (1<<7))==0)	returner |= (1<<5);

	//IR2 hinten rechts	
	//			   CBA
	PORTC  &= ~(0b01110000); 					// Modi umschalten
	_delay_us(2);
	if ((PINC & (1<<7))==0)	returner |= (1<<4); // Bei gedr�cktem Taster oder augel�stem IR speichern des Wertes 

	// Analog zu Schritt eins jetzt die folgenden Modi ohne Kommentare

	//Taster 1
	PORTC  |= (0b00010000); 
	_delay_us(2);
	if ((PINC & (1<<7))==0)	returner |= (1<<0);

	//Taster 2 
	PORTC  &= ~(0b01110000);
	PORTC  |=  (0b00100000); 
	_delay_us(2);
	if ((PINC & (1<<7))==0)	returner |= (1<<1);

	//IR 1 rechts
	PORTC  |= (0b00010000);   
	_delay_us(2);
	if ((PINC & (1<<7))==0)	returner |= (1<<3);
	
	//IR 4 vorne links
	PORTC  &= ~(0b01110000);
	PORTC  |=  (0b01000000); 
	_delay_us(2);
	if ((PINC & (1<<7))==0)	returner |= (1<<6);

	//IR 0 vorne Rechts
	PORTC  |= (0b00010000);   
	_delay_us(2);
	if ((PINC & (1<<7))==0)	returner |= (1<<2);

	return returner;
}

uint8_t multiplexer_taster() {


	return 0;	
}
	
#endif

