#ifndef BLUETOOTH_H
#define BLUETOOTH_H

////////////////////////////////////////////////////////////
//
// Initialisierung des zweiten UART
//
// Befehl: uart_bluetooth_init(); 
//
// Beschreibung:
//	 Initialisiert die UART1 mit:
//	 Baudrate 38400 Hertz (bei F_CPU 20MHz)
//	 8 Datenbits pro Zeichen, 1 Stopbit, ohne Parit�t, Asynchron
//
////////////////////////////////////////////////////////////
void uart_bluetooth_init(void)
{
	// Initialisierung des Bus	
	UCSR1A &= 0b11100000;		// Enable nothing, Disable U2X, MPCM(Multi-Processor Com. Mode), Reset errors
	UCSR1B  = 0b00011000;		// Enable RXCI/RX/TX; Disable TXCI/UDREI		//rxI disabled (bit7)
	UCSR1C  = 0b00000110;		// Async. UART, no parity, 8 data bits, 1 stop bit

	UBRR1H  = 0;				// Set UBRR = 32 (Baudrate 38400 Hz for F_CPU = 20MHz)
	UBRR1L  = 32;
	return;
}
void set_master(void) {
	eeprom_write_byte(((uint8_t*)1), bt_master);
}


#endif //BLUETOOTH_H
