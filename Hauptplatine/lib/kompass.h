#ifndef KOMPASS_H
#define KOMPASS_H

#include "i2cmaster.h"

///////////////////////////////////
// Abfragen des Kompasses 
// Auslesen des ersten Registers
// R�ckgabewert ein Byte 0-255 
////////////////////////////////// 

uint8_t kompass_abfragen_1byte(void)
{
	uint8_t kompasswert=0;

	cli(); 										// Ausschalten der Interrupts
    i2c_init(); 								// Initialisierung des I2C Bus
    i2c_start(Dev_kompass + I2C_WRITE); 		// Startbyte mit Adresse und Bit zum Schreiben
    i2c_write(0x01); 							// schreiben
	i2c_rep_start(Dev_kompass + I2C_READ);  	// Startbyte mit Adresse und Bit zum lesen 
    kompasswert = i2c_readNak();				// lesen                         
    i2c_stop(); 								// Stop des I2C Bus
	sei();										// Einschalten der Interrupts

	return kompasswert;							// R�ckgabe Kompasswert 
}

//////////////////
// Abfragen des Kompasses 
// Auslesen des zweiten und dritten Registers
// R�ckgabewert zwei Bytes erst High dann Low 0-3599 
//////////////////
uint16_t kompass_abfragen_2byte()
{
	uint16_t kompasswert_2bytes=0;

	cli();										// Ausschalten der Interrupts
    i2c_init();									// Initialisierung des I2C Bus
    i2c_start(Dev_kompass + I2C_WRITE); 		// Startbyte mit Adresse und Bit zum Schreiben
    i2c_write(0x02);							// schreiben
	i2c_rep_start(Dev_kompass + I2C_READ);		// Startbyte mit Adresse und Bit zum Lesen
    kompasswert_2bytes = (i2c_readAck()<<8);	// lesen 
    kompasswert_2bytes += i2c_readNak(); 		// lesen                         
    i2c_stop();									// Stop des I2C Bus
	sei();										// Einschalten der Interrupts
	
	return kompasswert_2bytes;					// R�ckgabe Kompasswert

}

//////////////////////////////
// Kompasskalibrierung
// Kalibrierung des Moduls durch senden verschiedener Register
// 1.Auf gew�nschte Null hinstellen und ersten Befehl senden an Register 0x22 und zwar 0x0F
// 2.Jetzt erste Kalibrierung durchf�hren mit 0xF5 
// 3.Danach um 90 Grad drehen und n�chsten befehl senden 0xF5 	
// 4.Wieder 90 Grad drehen wieder 0xF5
// 5.Wieder 90 Grad drehen wieder 0xF5
// Immer an Register 0x22 senden
// GEHT NOCH NICHT!!!!!!!!!!!!!
///////////////////////////////
/*uint8_t kompass_kalibrieren()
{
	cli();										// Ausschalten der Interrupts
    i2c_init();									// Initialisierung des I2C Bus
    i2c_start(Dev_kompass + I2C_WRITE); 		// Startbyte mit Adresse und Bit zum Schreiben
    i2c_write(0x22);							// schreiben
	i2c_write(0x0F);
	msleep(1000);
	i2c_write(0x22);
	i2c_write(0xF5);
	msleep(1000);
	i2c_write(0x22);
	i2c_write(0xF5);
	msleep(1000);
	i2c_write(0x22);
	i2c_write(0xF5);
	msleep(1000);
	i2c_write(0x22);
	i2c_write(0xF5);
	msleep(1000);
	i2c_stop();									// Stop des I2C Bus
	sei();
	
	return 1;	
}*/



#endif	//KOMPASS_H
