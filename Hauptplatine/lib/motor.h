#ifndef MOTOR_H
#define MOTOR_H


void motor(int16_t mx, int16_t my)
{
	mx = -mx;
	mo[2] =  mx+my; 
	mo[3] = -mx+my;
	mo[0] = -mx-my;
	mo[1] =  mx-my;
}

#endif //MOTOR_H

// gerichtetes Fahren
//        A
//       y|
//        |
// --------------->
//        |      x
//        |
//
// alpha = abweichung von vorne 0�
void regler_b(void) {
	int mx;
	int my;

	my = (60 - abs(ball_richtung/2))/(2);
	mx = ball_richtung;


	mo[2] = my+mx; 
	mo[3] = my+mx;
	mo[0] = -my+mx;
	mo[1] = -my+mx;

}
	

void regler_k(int mx, int my)
{
/*
	if(rambo) {
		mx = -mx;
		my = -my;
	}
	*/
/*
	if(mx > 100) mx = 100;
	if(mx < -100) mx = -100;
	if(my > 100) my = 100;
	if(my <-100) my = -100;
*/
    mx = -mx;
    uint8_t alpha_p=1, alpha_d=3, i; //alpha_i=0;
    int16_t faktor = 1<<3, alpha=0;
    
 
    /*************** Regler *****************/
    alpha       = (alpha_p * kompass_ist) + (kompass_ist-kompass_alt)*alpha_d; //+ (e_sum*alpha_i>>4);
    kompass_alt = kompass_ist;
 
    //// Zusammenrechnung der Reglergroesse mit den x - y Vorgaben f�r den Motor
 
    // Begrenzung des WinkelRegelanteils
    if (alpha >  110) alpha =  110;
    if (alpha < -110) alpha = -110;
 
    // Berechung das Minderungsfaktors noch fehlerhaft
/*
    if ((abs(mx)+abs(my)+abs(alpha)) > 220)
    {
        faktor = ((220-abs(alpha))/(abs(mx)+abs(my)))<<3;
    }else{
        faktor = 1<<3;
    }
*/
    /*************************************/
	mo[2] =  mx+my; 
	mo[3] = -mx+my;
	mo[0] = -mx-my;
	mo[1] =  mx-my;
	for(i=0;i<4;i++) mo[i] = (mo[i]*faktor>>3) - alpha;
}
