#include <stdint.h>
#ifndef SENSORIK_H
#define SENSORIK_H

uint8_t		motor_schalter=0;
uint8_t		schalter = 0;
//uint16_t	zaehler_fehler=0;
int8_t		tast;
uint8_t     i = 0, i2 = 0;

uint8_t		diptast=0;
uint8_t		dips=0;
uint8_t		taster=0;
uint8_t		irtast=0;
uint8_t		ir_v=0;
uint8_t		ir_h=0;

uint16_t	sharp[6]={0};	//2 nicht vorhanden
uint8_t		irfern[8]={0};	//
int16_t		fern_winkel=0;
uint8_t		fern_abstand=0;	

struct sharps {
uint16_t	links;
uint16_t	rechts;
uint16_t	vorne;
uint16_t	hinten;
} sharps = {0};

int16_t tordifferenz = 0;

int *komp_ptr = &kompass_ist;
					//
							//
							//
							//



unsigned char update_sensors() {
	uint16_t 	ir_sum=0;

	kompass_roh		=	i2c_lego_kompass();
	motor_schalter	=	schalter_abfragen();
	schalter		=   schalter_neu();
	if(display) tast			=	tastatur();
	diptast			=	multiplexer_abfragen();

//PORTC |= (1<<2); 	// Testpin ein	
	for(i=0;i<6;i++) sharp[i]		=	sharp_abfragen(i);//sharps abfragen
	sharps.vorne = sharp[1];
	sharps.rechts = sharp[2];
	sharps.hinten = sharp[3];
	sharps.links = sharp[5];
//PORTC &= ~(1<<2);  // Testpin aus

/****************** ir block ****************************/
//PORTC |= (1<<2); 	// Testpin ein
	memset(irfern, 0, sizeof(irfern));	//reset

	for(i2=0;i2<10;i2++) {

		irtast = multiplexer_ir_abfragen();
		ir_v   = ir_abfragen(6);
		ir_h   = ir_abfragen(7);

		for(i=2;i<8;i++) if(irtast & (1<<i)) irfern[i]++;
		if(ir_v) irfern[0]++;
		if(ir_h) irfern[1]++;

		_delay_us(70);

	}

	dips = ((diptast & 0b11111100) >>2);
	taster = ((diptast & 1<<0) << 3) | ((diptast & 1<<1) <<1) | ((irtast & 0b00000011)<<0);

	//BALLWINKEL

	ir_sum = irfern[0]+irfern[1]+irfern[2]+irfern[3]+irfern[4]+irfern[5]+irfern[6]+irfern[7];
	if(ir_sum < 6) fern_winkel = -200;
	//ball hinter bot
	else if(irfern[1]+irfern[7]+irfern[4] > 10) {
	//FILTER
		if(irfern[1] < 3) irfern[1] = 0;
		if(irfern[7] < 3) irfern[7] = 0;
		if(irfern[4] < 3) irfern[4] = 0;
		if(irfern[5] < 3) irfern[5] = 0;
		if(irfern[3] < 3) irfern[3] = 0;

		fern_winkel = (irfern[1]*SENSOR_1_ANGLE + irfern[7]*SENSOR_7_ANGLE_H + irfern[4]*SENSOR_4_ANGLE_H +
					   irfern[5]*SENSOR_5_ANGLE_H + irfern[3]*SENSOR_3_ANGLE_H)
					  /
					  (irfern[1] + irfern[7] + irfern[4] + irfern[5] + irfern[3]);
		//winkel zur�ck verschieben
		if(fern_winkel < 0) fern_winkel += 180;
		else 				fern_winkel -= 180;
	}
	//ball vor bot
	else {
		if(irfern[0] < 3) irfern[0] = 0;
		if(irfern[6] < 3) irfern[6] = 0;
		if(irfern[2] < 3) irfern[2] = 0;
		if(irfern[5] < 3) irfern[5] = 0;
		if(irfern[3] < 3) irfern[3] = 0;


		fern_winkel = (irfern[0]*SENSOR_0_ANGLE + irfern[6]*SENSOR_6_ANGLE + 
					   irfern[2]*SENSOR_2_ANGLE + irfern[5]*SENSOR_5_ANGLE + irfern[3]*SENSOR_3_ANGLE)
  					  /
					   (irfern[0] + irfern[6] + irfern[2] + irfern[5] + irfern[3]);
	}
//PORTC &= ~(1<<2);  // Testpin aus

	return 0;
}


int16_t uberschlag180(int16_t var) {
	if(var<(-180)) 	var += 360;
	if(var>180)		var -= 360;

	return var;
}
void modify_cv(int8_t offset) {
	kompass_vorne += offset;
	kompass_vorne = uberschlag180(kompass_vorne);
}

int16_t get_kompass_ist() {
	int16_t eingabe;

	eingabe		=	kompass_roh-kompass_vorne;
			
	eingabe = uberschlag180(eingabe);

	return eingabe;
}




#endif
