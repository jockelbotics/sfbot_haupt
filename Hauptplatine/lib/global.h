// ATMega644P - global.h

#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdbool.h>
#include <avr/eeprom.h>

#ifndef ROBO644_H
#define ROBO644_H

#define HAUPTPLATINE


//----------------------------------------------------------
// Definitionen der analogen Ports
//----------------------------------------------------------

// Variablen
volatile uint8_t display = 0;      // Displayzeiger 1 = angesteckt und 0 = abgezogen


// Funktionen

#include "analog.h"

//----------------------------------------------------------
// Definitionen Digitalports
//----------------------------------------------------------

// Variblen

#define ROT 5		// F�r das einschalten der LEDs den Farebennamen benutzen
#define GRUEN 4		// Rot entspricht 5 und Gr�n 4

// Funktionen
#include "digital.h"

//----------------------------------------------------------
// Definitionen Timer
//----------------------------------------------------------

// Variablen

volatile uint32_t zeit    = 0;     // Hundertstelsekunden
volatile uint32_t zeit_ms    = 0;  // Tausendstelsekunden
volatile uint32_t zeit_us    = 0;  // Tausendstelsekunden
uint32_t time_remaining = 0;	   // verbleibende zeit in loop
uint8_t  last_error_byte = 0;
int16_t  last_error_byte_val = 0;
int16_t  last_error_byte_val2 = 0;

uint8_t fsm_linie = 0;	//state

bool bt_master = false;

// Funktionen
#include "timer.h"



//----------------------------------------------------------
// Definitionen - Kompass
//----------------------------------------------------------

#define Dev_kompass  0xC0


int16_t     kompass_ist=0, kompass_alt=0, kompass_vorne=0, kompass_roh=0, kompass_alt, kompass_vorne_start;

// Funktionen

#include "kompass.h"
#include "lego.h"

//----------------------------------------------------------
// Definitionen - Display und Tastatur
//----------------------------------------------------------

//Variablen

int8_t mo[4]= {0};
char zeile1[21];
char zeile2[21];
char zeile3[21];
char zeile4[21];

bool unterspannung 	= false;
bool rambo	   		= false;
bool kicken	   		= false;
bool tormann 		= false;

bool motor_menu = false;

uint8_t seite = 0;

volatile uint8_t bt_buf[];
volatile uint8_t bt_index;
uint8_t			 bt_last;

int16_t ball_richtung;
int16_t ball_richtung_absolut;
uint8_t ball_entfernung;


// Funktionen

#include "motor.h"



// Interne UART - PB
#include "uart_intern.h"

//----------------------------------------------------------
// Definitionen - Display und Tastatur
//----------------------------------------------------------

#define Dev  0xC4

// Funktionen

#include "display_tastatur.h"
#include "sensorik.h"

//----------------------------------------------------------
// Definitionen Serielle Daten�bertragung
//----------------------------------------------------------

// Standard UART - Peter Fleury
// Funktionen

#include "uart.h"
#include "SUART.h"

/* define CPU frequency in Mhz here if not defined in Makefile */
#ifndef F_CPU
#define F_CPU 20000000	//20 MHz
#endif

/* 9600 baud */	//Sp�ter l�schen
//#define UART_BAUD_RATE      9600   

/* 38400 baud */ //Sp�ter l�schen
#define UART_BAUD_RATE1     38400



// Bluetooth UART
#include "bluetooth.h"

#include "display_ausgabe.h"

#endif

//----------------------------------------------------------
// Initialisierung des robo32-boards
//----------------------------------------------------------

void init(void)
{
	analog_init();

	// Initialisierung der Ein- und Ausg�nge
	DDRA = 0b00000000;
	DDRB = 0b00000111;	//Eing�nge 0 Ausg�nge 1
	DDRC = 0b01110000;
	DDRD = 0b10111010;
	
	//PD7 suart tx
	//PA0 suart rx
	//PC 6-4 + 2 schalter

	//Schalten von Ausg�ngen und Aktivieren der Pull-Ups
	PORTA =0b00000000;
	PORTB =0b11111000;	// Bei einem Ausgang bedeutet 1 das High setzen des Ausgangs
	PORTC =0b10001100;	// Bei einem Eingang bedeutet 1 das aktivieren der Pull-Ups
	PORTD =0b01001110;
	
	//uart_init(UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) );
	uart_intern_init();
	#ifdef HAUPTPLATINE 
	bus_buffer_init_haupt();
	#endif// HAUPTPLATINE
	#ifdef SENSORPLATINE 
	bus_buffer_init_sensor();
	#endif// SENSORPLATINE
	#ifdef BODENPLATINE 
	bus_buffer_init_boden();
	#endif// BODENPLATINE
	//uart1_init(UART_BAUD_SELECT(UART_BAUD_RATE1,F_CPU) );


	uart_bluetooth_init();
//	suart_init();
    timer_init();
    sei();
	msleep(100);


//	if (tastatur() == -2) 	display = 0; 	// Display nicht angeschlossen
//	else 					display = 1;	// Display angeschlossen
	i2c_init();
	if(i2c_start(Dev+I2C_WRITE)) display =0;
	else display = 1; 
	
    //i2c_write(0x00);
    //i2c_write(0x00);
	//i2c_stop();
	


}

