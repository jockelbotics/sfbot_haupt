// Robo32 - lego.h

#ifndef LEGO_H
#define LEGO_H

#include "i2cmaster.h"


struct accl
{
	short  tx;
	short  ty;
	short  tz;

	uint8_t ax_h;
	uint8_t ax_l;
	uint8_t ay_h;
	uint8_t ay_l;
	uint8_t az_h;
	uint8_t az_l;

  	int   ax;
  	int   ay;
  	int   az;
	int error;
} accl = { 0 };

struct gyro
{
	uint8_t gx_h;
	uint8_t gx_l;
	uint8_t gy_h;
	uint8_t gy_l;
	uint8_t gz_h;
	uint8_t gz_l;

  	int   gx;
  	int   gy;
  	int   gz;
	int error;
};

int16_t kompass_imu(void)
{
    int16_t eingabe = 0;
	i2c_init_lego();
    i2c_start(0x22); 
    i2c_write(0x4B); 
	i2c_rep_start(0x23);
	eingabe = (i2c_readAck() ) + (i2c_readNak() << 8)-180;
    i2c_stop();
	return eingabe;
}

void read_accl_imu(void)
{

//    int16_t eingabe = 0;
	i2c_init_lego();
    i2c_start(0x22); 
    i2c_write(0x42); 
	i2c_rep_start(0x23);

	accl.tx = i2c_readAck();
	accl.ty = i2c_readAck();
	accl.tz = i2c_readAck();

	accl.ax = i2c_readAck() + (i2c_readAck() << 8);
	accl.ay = i2c_readAck() + (i2c_readAck() << 8);
	accl.az = i2c_readAck() + (i2c_readNak() << 8);

//	return 0;
}

// Auslesen des Hitechnic-Kompass-Sensors
void i2c_lego_kompass_cal(void)
{
	i2c_init_lego();
    i2c_start(0x02); 
    i2c_write(0x41); 	
    i2c_write(0x43); 	
	i2c_stop();
}

int16_t i2c_lego_kompass(void)
{
    int16_t eingabe = 0;
	i2c_init_lego();
    i2c_start(0x02); 
    i2c_write(0x42); 
	i2c_rep_start(0x03);
	eingabe = (i2c_readAck() << 1 ) + i2c_readNak() - 180;
    i2c_stop();
	return eingabe;
}

#endif
