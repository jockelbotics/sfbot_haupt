#ifndef DISPLAY_AUSGABE_H
#define DISPLAY_AUSGABE_H

unsigned char printstr(uint8_t zeile, uint8_t pos, char zeichen[]) 
{
	if(zeile == 1) {
		//zeile1=.....
	}
	return 0;
}

unsigned stellenzahl_berechnen(unsigned x){
	unsigned i = 0;
	do{
		i++;
	}while(x /= 10);
	return i;
}

/*
unsigned zahlenVerketten(unsigned i, unsigned j){
	unsigned stellenzahl = stellenzahl_berechnen(j);

	return i * pow(10, stellenzahl) + j;

}
*/
unsigned char printnbr(int32_t var, uint8_t zeile, uint8_t offset) 
{
	char buffer[11];
	itoa(var, buffer, 10);

	if(zeile == 1) {

		//scan available space/pos
		//
		for(i=0;i<10;i++) {
			if(!buffer[i]) break;
			else zeile1[i+offset] = buffer[i];
		}
	}
	if(zeile == 2) {

		for(i=0;i<10;i++) {
			if(!buffer[i]) break;
			else zeile2[i+offset] = buffer[i];
		}
	}
	if(zeile == 3) {

		for(i=0;i<10;i++) {
			if(!buffer[i]) break;
			else zeile3[i+offset] = buffer[i];
		}
	}
	if(zeile == 4) {

		for(i=0;i<10;i++) {
			if(!buffer[i]) break;
			else zeile4[i+offset] = buffer[i];
		}
	}
	return 0;
}


unsigned char display_ausgabe(void) 
{
	static	uint8_t		zeitscheibe=0;
	static  uint8_t 	menue[2]={0, 1};			// Speicherung Menue und Umtermenuenummern
	static 	uint8_t 	offset = 0;					// Variable f�r Offset der IR-Ball-Sensoren
	static  uint8_t     richtung = 0;				// Hilfsvariable im Motormenue
	static  uint8_t     tastold = 0;				// Hilfsvariable zur Steuerung des Displaymenues
	static  uint8_t     error_anzahl=0; 			// Z�hler f�r die Buserrors
	static  uint8_t 	bp_sensor=0;				// Selektierter Sensor
	static  uint8_t 	bp_sensor_wert=0;			// Korrespondierender Wert
	static  char		wert[3] = {0};				// string
	static  uint8_t		sw=0;						// Variable f�r den Schwellwert
	static  uint8_t		bodenwerte[3][8] = {{0}}; 	// Feld (Speichert Min, Max und Mittelwert)
	static  uint8_t		start=0;
	static	uint8_t		temp=0;						// u.a. Bodensensor Menu Funktion
	char 		buffer[11];					// Hilfsvariable zur Konvertierung von Zahlen in Text
	int16_t 	tmp = 0;					// Hilfsvariable im Motormenue
	uint8_t				bp_sensor_control = 0;

	if(start ==0)
	{
		for(i=0;i<8;i++) 
		{
			bodenwerte[0][i]=250;
			bodenwerte[1][i]=0;
			bodenwerte[2][i]=0;
		}
		start =1;
	}
	

	if (ui_buffer_send[38] != 0)		// gesetzt vom letzten Durchgang? Nullen, damit es nicht wieder gesendet und unten st�ndig neu gesetzt wird.
	{
		ui_buffer_send[38] = 0;
		ui_buffer_send[39] = 0;
	}

	//led_toggle(GRUEN);

	//Befehle zum Steuern durchs Displaymenue
								
	if (tast == 42 && tastold !=42 && menue[1] == 1)   {menue[0]=0;menue[1]=1;} // zurueck ins Hauptmenue
	if (tast == 42 && tastold !=42 && menue[1] != 1 && menue[1] >0)	menue[1]--;	// im Untermenue zur�ckspringen	
	if (tast == 35 && tastold !=35 && menue[0] != 0) 	menue[1]++;				// im Untermenue vorspringen
	if (tast > 0   && tast     < 10 && menue[0] == 0) 	menue[0] = tast;		// ein Menue betreten
		
	// Menueaufrufe
	switch (menue[0])
	{
		case 0: 	// Hauptmenue
			strcpy(zeile1,"*.HM  ;1.Bus ;2.HP  ");
			strcpy(zeile2,"3.SP  ;4.BP  ;5.MP  ");
			strcpy(zeile3,"6.KP  ;7-    ;8.BT  ");
			break;

		case 1:		//// Busmenue
			switch (menue[1])
			{
				//1.Untermenue
				case 1:
					strcpy(zeile1,"                    ");
					strcpy(zeile2,"                    ");
					strcpy(zeile3,"                    ");
					//Motoren im Bus
					printnbr(ui_buffer_recieve[V_MOT0], 1,  0);			//Motor 0
					printnbr(ui_buffer_recieve[V_MOT1], 1,  4);			//Motor 1
					printnbr(ui_buffer_recieve[V_MOT2], 1,  8);			//Motor 2
					printnbr(ui_buffer_recieve[V_MOT3], 1, 12);			//Motor 3
					printnbr(ui_buffer_recieve[K_MOT] , 1, 16);			//Geschwindigkeit
					//
					printnbr(ui_buffer_recieve[BALLWINKEL]    , 2,  0);	//Ballwinkel
					printnbr(ui_buffer_recieve[BALLENTFERNUNG], 2,  4);	//Ballentfernung
					printnbr(ui_buffer_recieve[BP_ANALOG_0]   , 2,  8);	//Bodensensor 0
					printnbr(ui_buffer_recieve[BP_ANALOG_1]   , 2, 12);	//Bodensensor 1
					printnbr(ui_buffer_recieve[BP_ANALOG_2]   , 2, 16);	//Bodensensor 2
					break;

				//2.Untermenue
				case 2:
					strcpy(zeile1,"1.2.Bus             ");
					strcpy(zeile2,"                    ");
					strcpy(zeile3,"                    ");
					break;
						
				//3.Untermenue
				case 3: 
					strcpy(zeile1,"1.3.Bus             ");
					strcpy(zeile2,"                    ");
					strcpy(zeile3,"                    ");
					break;
			
				default: 
					menue[1]=1;		
					break;

			}
			break;
										
		case 2:		//// Hauptplatinenmenue
			switch (menue[1])
			{
				//1.Untermenue
				case 1:
					strcpy(zeile1,"2.1.HP  FW          ");
					strcpy(zeile2,"T     S             ");
					strcpy(zeile3,"D                   ");

					printnbr(fern_winkel, 1, 11);

					printnbr(last_error_byte_val, 3, 12);
					printnbr(last_error_byte_val2, 3, 8);


					if (irtast & (1<<0)) 	zeile2[1]=49;		//Taster1
					else 					zeile2[1]=48;
					if (irtast & (1<<1)) 	zeile2[2]=49;		//Taster2
					else 					zeile2[2]=48;
					if (diptast & (1<<1)) 	zeile2[3]=49;		//Taster3
					else 					zeile2[3]=48;
					if (diptast & (1<<0)) 	zeile2[4]=49;		//Taster4
					else 					zeile2[4]=48;

					if (motor_schalter==1) 		zeile2[7]=49;		//Schalter0 (Motorschlter) 
					else 						zeile2[7]=48;
					if (schalter & SCHALTER1) 	zeile2[8]=49;		//Schalter1 
					else 						zeile2[8]=48;
					if (schalter & SCHALTER2) 	zeile2[9]=49;		//Schalter2
					else 						zeile2[9]=48;
					if (schalter & SCHALTER3) 	zeile2[10]=49;		//Schalter3
					else 						zeile2[10]=48;
					if (schalter & SCHALTER4) 	zeile2[11]=49;		//Schalter4
					else 						zeile2[11]=48;

					if (diptast & (1<<2))	zeile3[1]=49;		//Dip1 
					else 					zeile3[1]=48;
					if (diptast & (1<<3)) 	zeile3[2]=49;		//Dip2
					else 					zeile3[2]=48;
					if (diptast & (1<<4)) 	zeile3[3]=49;		//Dip3
					else 					zeile3[3]=48;
					if (diptast & (1<<5)) 	zeile3[4]=49;		//Dip4
					else 					zeile3[4]=48;
					if (diptast & (1<<6)) 	zeile3[5]=49;		//Dip5 
					else 					zeile3[5]=48;
					if (diptast & (1<<7)) 	zeile3[6]=49;		//Dip6
					else 					zeile3[6]=48;

					if(tast>0) 	offset = tast;					//Steuerung des Offsets der Ballsensoren
					else 		offset = 0;
			
					//IR-Ball-Fernsensoren als Ring ausgeben
					if(irfern[0]>offset) printnbr(irfern[0], 1, 17);	//vorne
					if(irfern[1]>offset) printnbr(irfern[1], 3, 17);	//hinten
					if(irfern[2]>offset) printnbr(irfern[2], 1, 18);	//vorne rechts
					if(irfern[3]>offset) printnbr(irfern[3], 2, 18);	//rechts
					if(irfern[4]>offset) printnbr(irfern[4], 3, 18);	//hinten rechts
					if(irfern[7]>offset) printnbr(irfern[7], 3, 16);	//hinten links
					if(irfern[6]>offset) printnbr(irfern[6], 1, 16);	//vorne links
					if(irfern[5]>offset) printnbr(irfern[5], 2, 16);	//links						
					break;
				
				//2.Untermenue
				case 2:
					strcpy(zeile1,"2.2.HP              ");
					strcpy(zeile2,"                    ");
					strcpy(zeile3,"                    ");

			//		printnbr(sharp[0], 3, 1);		//nicht angeschlossen
					printnbr(sharp[1], 1, 9);
					printnbr(sharp[2], 2, 14);
					printnbr(sharp[3], 3, 9);
			//		printnbr(sharp[4], 2, 12);		//nicht angeschlossen
					printnbr(sharp[5], 2, 4);
					printnbr(time_remaining, 3, 16);
					printnbr(zeit_ms, 1, 14);
					break;

				//3.Untermenue
				case 3:
					strcpy(zeile1,"tordifferenz        ");
					strcpy(zeile2,"                    ");
					strcpy(zeile3,"                    ");

					printnbr(tordifferenz, 2, 1);
					printnbr(seite, 3, 1);
					printnbr(fsm_linie, 3, 5);					

					break;
				default: 
					menue[1]=1;
					break;
			}
			break;

		case 3:		////Sensorplatine
			switch (menue[1])
			{
				//1.Untermenue
				case 1:
					strcpy(zeile1,"3.1.SP              ");
					strcpy(zeile2,"W                   ");
					strcpy(zeile3,"E                   ");

			//			tmp = ((ui_buffer_recieve[BALLENTFERNUNG] & 0b10000000) << 8);
					tmp = (ui_buffer_recieve[BALLWINKEL]); 
					if(ui_buffer_recieve[BALLENTFERNUNG] & 0b10000000) tmp = -tmp;
			//		tmp = 1 << 15;
					
					printnbr(tmp, 2, 2);
					printnbr((ui_buffer_recieve[BALLENTFERNUNG] & ~(1<<7)), 3, 2);


					break;

				//2.Untermenue
				case 2:
					strcpy(zeile1,"3.2.SP              ");
					strcpy(zeile2,"                    ");
					strcpy(zeile3,"                    ");


					itoa(tmp, buffer, 2);
					for(int i=0;i<10;i++)
					{
						if (buffer[i]==0) break;
						zeile2[i]=buffer[i];
					}


					printnbr(ball_richtung, 3, 2);

					break;
						
				default: 
					menue[1]=1;
					break;
			}
			break;

		case 4:		//// Bodenplatine
			switch (menue[1])
			{
				//1.Untermenue
				case 1:
					strcpy(zeile1," 4.1.BP             ");
					strcpy(zeile2,"                    ");
					strcpy(zeile3,"                    ");
					if (ui_buffer_recieve[BODENSENSOREN] & (1<<6))	zeile1[17]=49;		//Bodensensoren vorne bin�r	(6) 
					else 					zeile1[17]=48;
					if (ui_buffer_recieve[BODENSENSOREN] & (1<<0)) 	zeile2[16]=49;		//Bodensensoren links bin�r	(0)
					else 					zeile2[16]=48;
					if (ui_buffer_recieve[BODENSENSOREN] & (1<<4)) 	zeile2[18]=49;		//Bodensensoren rechts bin�r(4)
					else 					zeile2[18]=48;
					if (ui_buffer_recieve[BODENSENSOREN] & (1<<3)) 	zeile3[17]=49;		//Bodensensor hinten bin�r	(3)
					else 					zeile3[17]=48;

					printnbr(ui_buffer_recieve[BP_ANALOG_0+1],	2,  0);					//Bodensensor 0
					printnbr(ui_buffer_recieve[BP_ANALOG_1+1],	2,  4);					//Bodensensor 1						
					printnbr(ui_buffer_recieve[BP_ANALOG_2+1], 	2,  8);					//Bodensensor 2						
					printnbr(ui_buffer_recieve[BP_ANALOG_3+1], 	2, 12);					//Bodensensor 3

					printnbr(ui_buffer_recieve[BP_ANALOG_4+1], 	3,  0);					//Bodensensor 4											
					printnbr(ui_buffer_recieve[BP_ANALOG_5+1], 	3,  4);					//Bodensensor 5						
					printnbr(ui_buffer_recieve[BP_ANALOG_6+1], 	3,  8);					//Bodensenor  6						
					printnbr(ui_buffer_recieve[BP_ANALOG_7+1], 	3, 12);					//Bodensensor 7	
					
					printnbr(ui_buffer_recieve[40], 	1,  8);
					printnbr(ui_buffer_recieve[41], 	1,  12);

					break;

				//2.Untermenue
				case 2:
					strcpy(zeile1,"     Kalibrieren    ");
					strcpy(zeile2,"   |   |   |   |    ");
					strcpy(zeile3,"   |   |   |        ");

					printnbr(ui_buffer_recieve[BP_ANALOG_1], 2,  0);					//Bodensensor 1	(zeile 2)											
					printnbr(ui_buffer_recieve[BP_ANALOG_2], 2,  4);					//Bodensensor 2	(zeile 2)			
					printnbr(ui_buffer_recieve[BP_ANALOG_3], 2,  8);					//Bodensensor 3	(zeile 2)
					printnbr(ui_buffer_recieve[BP_ANALOG_4], 2,  12);					//Bodensensor 4 (zeile 2)
																
					printnbr(ui_buffer_recieve[BP_ANALOG_5], 3,  0);					//Bodensensor 5 (zeile 3)
					printnbr(ui_buffer_recieve[BP_ANALOG_6], 3,  4);					//Bodensensor 6 (zeile 3)	
					printnbr(ui_buffer_recieve[BP_ANALOG_7], 3,  8);					//Bodensensor 7 (zeile 3)

					if (ui_buffer_recieve[BODENSENSOREN] & (1<<6))	zeile1[17]=49;		//Bodensensoren vorne bin�r	(6) 
					else 					zeile1[17]=48;
					if (ui_buffer_recieve[BODENSENSOREN] & (1<<0)) 	zeile2[16]=49;		//Bodensensoren links bin�r	(0)
					else 					zeile2[16]=48;
					if (ui_buffer_recieve[BODENSENSOREN] & (1<<4)) 	zeile2[18]=49;		//Bodensensoren rechts bin�r(4)
					else 					zeile2[18]=48;
					if (ui_buffer_recieve[BODENSENSOREN] & (1<<3)) 	zeile3[17]=49;		//Bodensensor hinten bin�r	(3)
					else 					zeile3[17]=48;

					ui_buffer_send[38] = 0;
					if (tast != 42 && tast != 35 && tast != 0 && tast > 0 && tast != tastold) {
						menue[1]++;
						bp_sensor = tast;
					}

					temp = 0;
					sw = 0; // Merkvariable zur�cksetzen, bevor der n�chste Bodensensor Schwellwert angepasst wird
					break;

				//3.Untermenue
				case 3:
					strcpy(zeile1,"Kalibrieren von     ");
					strcpy(zeile2,"Min:    Max:   A:   ");
					strcpy(zeile3,"Cur:    SW:         ");

					//speichern von Minimum, Maximum und Mittelwert in das Feld bodenwerte f�r den gew�hlten Bodensensor
					if(ui_buffer_recieve[BP_ANALOG_0+bp_sensor]<bodenwerte[0][bp_sensor])	bodenwerte[0][bp_sensor]=ui_buffer_recieve[BP_ANALOG_0+bp_sensor];
					if(ui_buffer_recieve[BP_ANALOG_0+bp_sensor]>bodenwerte[1][bp_sensor])	bodenwerte[1][bp_sensor]=ui_buffer_recieve[BP_ANALOG_0+bp_sensor];
					bodenwerte[2][bp_sensor]=(bodenwerte[1][bp_sensor]+bodenwerte[0][bp_sensor])/2;
	
				
					printnbr(bodenwerte[1][bp_sensor],2,12);					//Maximum anzeigen
					printnbr(bodenwerte[0][bp_sensor],2,4);						//Minimum anzeigen
					printnbr(sw,3,11);											//SW anzeigen
					printnbr(bodenwerte[2][bp_sensor],2,17);					//Average/Mittelwert anzeigen			
					printnbr(ui_buffer_recieve[BP_ANALOG_0+bp_sensor],3,4);		//Current(aktueller) Wert anzeigen
				
					printnbr(bp_sensor, 1, 16);		//Zahl von ausgew�hltem Bodensensor
				


					if (temp == 0)								//Wenn SW nicht ge�ndert wird -> sw = Mittelwert
					{
						temp = 1;	
						sw = bodenwerte[2][bp_sensor];	
					}
					if(tast == 2 && tast != tastold)			//Mit Taste 2 -> Schwellwert erh�hen
					{
						if (sw < 250)	sw++;
					}
					else if(tast == 8 && tast != tastold)		//Mit Taste 8 -> Schwellwert verringern
					{
						if (sw > 1) sw--;
					}
					else if(tast == 0 && tast != tastold)		//Mit Taste 0 -> SW zur�cksetzen auf Average (s.o.)
					{
						temp = 0;
					}
					else if(tast == 1)							//Mit Taste 1 = Aktuellen Schwellwert anzeigen / hochsenden
					{
						bp_sensor_control |= (1 << 4);

					}



																						

					ui_buffer_send[38] = bp_sensor | bp_sensor_control;		//Senden des Bodensensorindex	
					ui_buffer_send[39] = sw;								//Senden des Schwellwerts
				//	bp_sensor_control = 0;
/*
					if(tast >= 0 && tast <= 9 &&  tast != tastold) {
						for(i=0;i<3;i++) {
							if(wert[i] == 0) {
								wert[i] = tast;
								break;

							}
						}
					}
*/			
/*
					if(tast == 35 && wert[0]+wert[1]) {
						for(i=0;i<3;i++) wert[i] = 0;
						tast = -2;
					}
*/					
					break;

				//4.Untermenue
				case 4:
					strcpy(zeile1,"Schwellwert         ");
					strcpy(zeile2,"gesetzt f�r         ");
					strcpy(zeile3,"Press any Key       ");

					printnbr(sw, 1, 12);
					printnbr(bp_sensor, 2, 12);
					ui_buffer_send[38] = bp_sensor | (1<<5);
					ui_buffer_send[39] = sw;


					if(tast != -2 && tast != 42) {
						menue[1] = 1;
						sw = 0;
						bp_sensor = 0;
						memset(wert, 0, sizeof(wert));
					}
					break;
/*

				case 5:
					strcpy(zeile1,"   |   |   |   |    ");
					strcpy(zeile2,"   |   |   |   |    ");
					strcpy(zeile3,"   |   |   |   |    ");

					//ui_buffer_recieve[BP_ANALOG_2]=100;
					//ui_buffer_recieve[BP_ANALOG_3]=50;

					// Wertereset --> aktuelle Bodensensorwerte setzen
					if (tast == 0)
					{
						for (i=1; i<8; i++)
						{
							bodenwerte[0][i] = ui_buffer_recieve[BP_ANALOG_0+i];
							bodenwerte[1][i] = ui_buffer_recieve[BP_ANALOG_0+i];
						}
					}

					//speichern von Minimum, Maximum und Mittelwert in das Feld bodenwerte
					for(i=0;i<8;i++) 
					{	
						if(ui_buffer_recieve[BP_ANALOG_1+i]<bodenwerte[0][i])	bodenwerte[0][i]=ui_buffer_recieve[BP_ANALOG_1+i];
						if(ui_buffer_recieve[BP_ANALOG_1+i]>bodenwerte[1][i])	bodenwerte[1][i]=ui_buffer_recieve[BP_ANALOG_1+i];
																				bodenwerte[2][i]=(bodenwerte[1][i]+bodenwerte[0][i])/2;
					}
			
					//Ausgabe der Feldwerte aus bodenwerte
					printnbr(bodenwerte[0][0], 1, 0);
					printnbr(bodenwerte[0][1], 1, 4);
					printnbr(bodenwerte[0][2], 1, 8);
					printnbr(bodenwerte[0][3], 1, 12);

					printnbr(bodenwerte[1][0], 2, 0);
					printnbr(bodenwerte[1][1], 2, 4);
					printnbr(bodenwerte[1][2], 2, 8);
					printnbr(bodenwerte[1][3], 2, 12);

					printnbr(bodenwerte[2][0], 3, 0);
					printnbr(bodenwerte[2][1], 3, 4);
					printnbr(bodenwerte[2][2], 3, 8);
					printnbr(bodenwerte[2][3], 3, 12);

																
					if(tast==(49|50|51|52|53|54|55|56)) {
															//erh�hen des Schwellwerts durch +
															//best�tigen
															//erniedrigen des Schwellwerts durch -
														}
					else{
							if (ui_buffer_recieve[BODENSENSOREN] & (1<<6))	zeile1[17]=49;		//Bodensensoren vorne bin�r	(6) 
							else 					zeile1[17]=48;
							if (ui_buffer_recieve[BODENSENSOREN] & (1<<0)) 	zeile2[16]=49;		//Bodensensoren links bin�r	(0)
							else 					zeile2[16]=48;
							if (ui_buffer_recieve[BODENSENSOREN] & (1<<4)) 	zeile2[18]=49;		//Bodensensoren rechts bin�r(4)
							else 					zeile2[18]=48;
							if (ui_buffer_recieve[BODENSENSOREN] & (1<<3)) 	zeile3[17]=49;		//Bodensensor hinten bin�r	(3)
							else 					zeile3[17]=48;
						}
						break;
*/
			/*	case 6:
					strcpy(zeile1,"   |   |   |   |    ");
					strcpy(zeile2,"   |   |   |   |    ");
					strcpy(zeile3,"   |   |   |   |    ");




					if(tast==(49|50|51|52|53|54|55|56)) {
							printnbr(49, 1, 16);			//erh�hen des Schwellwerts durch +
							printnbr(50, 2, 16);			//best�tigen
							printnbr(51, 3, 16);			//erniedrigen des Schwellwerts durch -
														}
					else{
							if (ui_buffer_recieve[BODENSENSOREN] & (1<<6))	zeile1[17]=49;		//Bodensensoren vorne bin�r	(6) 
							else 					zeile1[17]=48;
							if (ui_buffer_recieve[BODENSENSOREN] & (1<<0)) 	zeile2[16]=49;		//Bodensensoren links bin�r	(0)
							else 					zeile2[16]=48;
							if (ui_buffer_recieve[BODENSENSOREN] & (1<<4)) 	zeile2[18]=49;		//Bodensensoren rechts bin�r(4)
							else 					zeile2[18]=48;
							if (ui_buffer_recieve[BODENSENSOREN] & (1<<3)) 	zeile3[17]=49;		//Bodensensor hinten bin�r	(3)
							else 					zeile3[17]=48;
						}
					
								

					
					
					break;*/


				default: 
					menue[1]=1;
					break;
			}
			break;
					

		case 5:		//// Motorplatinen
		
			motor_menu = true;
			
			switch (menue[1])
			{
				//1.Untermenue
				case 1:
					strcpy(zeile1,"5.1.MP  Fahrentest  ");
					strcpy(zeile2,"                    ");
					strcpy(zeile3,"                    ");
					
					if (ui_buffer_recieve[VIMOT0] & 0b10000000) tmp = ui_buffer_recieve[VIMOT0] & (0b01111111);
					else 										tmp = ui_buffer_recieve[VIMOT0];							
					printnbr(tmp, 2,  0);
					if (ui_buffer_recieve[VIMOT1] & 0b10000000) tmp = ui_buffer_recieve[VIMOT1] & (0b01111111);
					else 										tmp = ui_buffer_recieve[VIMOT1];							
					printnbr(tmp, 2,  4);
					if (ui_buffer_recieve[VIMOT2] & 0b10000000) tmp = ui_buffer_recieve[VIMOT2] & (0b01111111);
					else 										tmp = ui_buffer_recieve[VIMOT2];							
					printnbr(tmp, 2,  8);
					if (ui_buffer_recieve[VIMOT3] & 0b10000000) tmp = ui_buffer_recieve[VIMOT3] & (0b01111111);
					else 										tmp = ui_buffer_recieve[VIMOT3];							
					printnbr(tmp, 2, 12);

					printnbr(ui_buffer_recieve[SMOT0A], 3,  0);
					printnbr(ui_buffer_recieve[SMOT1A], 3,  4);
					printnbr(ui_buffer_recieve[SMOT2A], 3,  8);
					printnbr(ui_buffer_recieve[SMOT3A], 3, 12);

				

					switch (tast)
					{	//Tastenbelegung f�r Fahrentest
						case 1:					// 1 diagonal links vorne
							motor(-25, 25);
							break;
						case 2:					// 2 vorwaerts
							motor(0, 25);
							break;
						case 3:					// 3 diagonal rechts vorne
							motor(25, 25);
							break;
						case 4:					// 4 links
							motor(-25, 0);
							break;
						case 5:					// 5 stopp
							motor(0,0);
							break;
						case 6:					// 6 rechts
							motor(25, 0);
							break;
						case 7:					// 7 diagonal links hinten
							motor(-25, -25);
							break;
						case 8:					// 8 r�ckwaerts
							motor(0, -25);
							break;
						case 9:					// 9 diagonal rechts hinten
							motor(25, -25);
							break;
						case 0:					// 0 drehen gegen uhrzeigersinn
							motor(0,0);
							break; 
					} 
					break;

				//2.Untermenue
				case 2:
					// Test Motor0
					//
					// max Strom einstellen: 1 = 50       | 2 = 100 | 3 = 200
					// Richtungswahl:        4 = vorw�rts | 5= aus  | 6 = r�ckw�rts
					// Geschwindigkeitswahl: 7 = 30       | 8 = 60  | 9 = 90
				
					strcpy(zeile1,"5.2.MP Test Motor 0 ");
					strcpy(zeile2,"HP:     V     S    ");
					strcpy(zeile3,"MP: V      S        ");

					// Zeile 2 Hauptplatine
					if (ui_buffer_send[V_MOT0] & 0b10000000) 	tmp = ui_buffer_send[V_MOT0] & (0b01111111);
					else 										tmp = ui_buffer_send[V_MOT0];							
					printnbr(tmp, 2,  4);
					if (ui_buffer_recieve[V_MOT0] & 0b10000000) tmp = ui_buffer_recieve[V_MOT0] & (0b01111111);
					else 										tmp = ui_buffer_recieve[V_MOT0];							
					printnbr(tmp, 2,  9);
					printnbr(ui_buffer_recieve[K_MOT], 2,  15);
					printnbr(richtung, 2,  19);

					// Zeile 3 Motorplatine
					if (ui_buffer_recieve[VIMOT0] & 0b10000000) tmp = ui_buffer_recieve[VIMOT0] & (0b01111111);
					else 										tmp = ui_buffer_recieve[VIMOT0];							
					printnbr(tmp, 3,  5);
				
					printnbr(ui_buffer_recieve[SMOT0A], 3,  12);
							
					mo[1] = 0;
					mo[2] = 0;
					mo[3] = 0;


					switch (tast)
					{
						case 1:					
							ui_buffer_send[K_MOT]=50;		// Motorleistung = 50
							break;
						case 2:					
							ui_buffer_send[K_MOT]=100;		// Motorleistung = 100
							break;
						case 3:					
							ui_buffer_send[K_MOT]=200;		// Motorleistung = 200
							break;
						case 4:					
							richtung = 0;						// vorw�rts
							mo[0]= 0;
							break;
						case 5:					
							mo[0] = 0;
							
							// Motor aus
							break;
						case 6:				
							richtung = 1; 
							mo[0]= 0;						// r�ckw�rts
							break;
						case 7:					
							if (richtung) 	mo[0]= -30;
							else  			mo[0]=  30;
							break;
						case 8:					
							if (richtung) 	mo[0]= -60;
							else  			mo[0]=  60;
							break;
						case 9:				
							if (richtung) 	mo[0]= -90;
							else  			mo[0]=  90;
							break;
					} 
					break;
				
				//3.Untermenue	
				case 3:
					// Test Motor1
					//
					// max Strom einstellen: 1 = 50       | 2 = 100 | 3 = 200
					// Richtungswahl:        4 = vorw�rts | 5= aus  | 6 = r�ckw�rts
					// Geschwindigkeitswahl: 7 = 30       | 8 = 60  | 9 = 90
				
					strcpy(zeile1,"5.3 MP Test Motor 1 ");
					strcpy(zeile2,"HP: V      S        ");
					strcpy(zeile3,"MP: V      S        ");

					// Zeile 2 Hauptplatine
					if (ui_buffer_recieve[V_MOT1] & 0b10000000) tmp = ui_buffer_recieve[V_MOT1] & (0b01111111);
					else 										tmp = ui_buffer_recieve[V_MOT1];							
					printnbr(tmp, 2,  5);
				
					printnbr(ui_buffer_recieve[K_MOT], 2,  12);
					printnbr(richtung, 2,  19);

					// Zeile 3 Motorplatine
					if (ui_buffer_recieve[VIMOT1] & 0b10000000) tmp = ui_buffer_recieve[VIMOT1] & (0b01111111);
					else 										tmp = ui_buffer_recieve[VIMOT1];							
					printnbr(tmp, 3,  5);
				
					printnbr(ui_buffer_recieve[SMOT1A], 3,  12);
							
					mo[0] = 0;
					mo[2] = 0;
					mo[3] = 0;


					switch (tast)
					{	//Tastenbelegung f�r Test Motor 1
						case 1:					
							ui_buffer_send[K_MOT]=50;		// Motorleistung = 50
							break;
						case 2:					
							ui_buffer_send[K_MOT]=100;		// Motorleistung = 100
							break;
						case 3:					
							ui_buffer_send[K_MOT]=200;		// Motorleistung = 200
							break;
						case 4:					
							richtung = 0;					// vorw�rts
							mo[1]= 0;
							break;
						case 5:					
							mo[1] = 0;						// Motor aus
							break;
						case 6:				
							richtung = 1; 
							mo[1]= 0;						// r�ckw�rts
							break;
						case 7:					
							if (richtung) 	mo[1]= -30;
							else  			mo[1]=  30;
							break;
						case 8:					
							if (richtung) 	mo[1]= -60;
							else  			mo[1]=  60;
							break;
						case 9:				
							if (richtung) 	mo[1]= -90;
							else  			mo[1]=  90;
							break;
					} 
					break;

				//4.Untermenue
				case 4:
					// Test Motor2
					//
					// max Strom einstellen: 1 = 50       | 2 = 100 | 3 = 200
					// Richtungswahl:        4 = vorw�rts | 5= aus  | 6 = r�ckw�rts
					// Geschwindigkeitswahl: 7 = 30       | 8 = 60  | 9 = 90
				
					strcpy(zeile1,"5.4.MP Test Motor 2 ");
					strcpy(zeile2,"HP: V      S        ");
					strcpy(zeile3,"MP: V      S        ");

					// Zeile 2 Hauptplatine
					if (ui_buffer_recieve[V_MOT2] & 0b10000000) tmp = ui_buffer_recieve[V_MOT2] & (0b01111111);
					else 										tmp = ui_buffer_recieve[V_MOT2];							
					printnbr(tmp, 2,  5);
				
					printnbr(ui_buffer_recieve[K_MOT], 2,  12);
					printnbr(richtung, 2,  19);

					// Zeile 3 Motorplatine
					if (ui_buffer_recieve[VIMOT2] & 0b10000000) tmp = ui_buffer_recieve[VIMOT2] & (0b01111111);
					else 										tmp = ui_buffer_recieve[VIMOT2];							
					printnbr(tmp, 3,  5);
				
					printnbr(ui_buffer_recieve[SMOT2A], 3,  12);
							
					mo[0] = 0;
					mo[1] = 0;
					mo[3] = 0;


					switch (tast)
					{	//Tastenbelegung f�r Test Motor 2
						case 1:					
							ui_buffer_send[K_MOT]=50;		// Motorleistung = 50
							break;
						case 2:					
							ui_buffer_send[K_MOT]=100;		// Motorleistung = 100
							break;
						case 3:					
							ui_buffer_send[K_MOT]=200;		// Motorleistung = 200
							break;
						case 4:					
							richtung = 0;					// vorw�rts
							mo[2]= 0;
							break;
						case 5:					
							mo[2] = 0;						// Motor aus
							break;
						case 6:				
							richtung = 1; 
							mo[2]= 0;						// r�ckw�rts
							break;
						case 7:					
							if (richtung) 	mo[2]= -30;
							else  			mo[2]=  30;
							break;
						case 8:					
							if (richtung) 	mo[2]= -60;
							else  			mo[2]=  60;
							break;
						case 9:				
							if (richtung) 	mo[2]= -90;
							else  			mo[2]=  90;
							break;
					} 
					break;
				case 5:
					// Test Motor3
					//
					// max Strom einstellen: 1 = 50       | 2 = 100 | 3 = 200
					// Richtungswahl:        4 = vorw�rts | 5= aus  | 6 = r�ckw�rts
					// Geschwindigkeitswahl: 7 = 30       | 8 = 60  | 9 = 90
				
					strcpy(zeile1,"5.5.MP Test Motor 3 ");
					strcpy(zeile2,"HP: V      S        ");
					strcpy(zeile3,"MP: V      S        ");

					// Zeile 2 Hauptplatine
					if (ui_buffer_recieve[V_MOT3] & 0b10000000) tmp = ui_buffer_recieve[V_MOT3] & (0b01111111);
					else 										tmp = ui_buffer_recieve[V_MOT3];							
					printnbr(tmp, 2,  5);
				
					printnbr(ui_buffer_recieve[K_MOT], 2,  12);
					printnbr(richtung, 2,  19);

					// Zeile 3 Motorplatine
					if (ui_buffer_recieve[VIMOT3] & 0b10000000) tmp = ui_buffer_recieve[VIMOT3] & (0b01111111);
					else 										tmp = ui_buffer_recieve[VIMOT3];							
					printnbr(tmp, 3,  5);
				
					printnbr(ui_buffer_recieve[SMOT3A], 3,  12);
							
					mo[0] = 0;
					mo[1] = 0;
					mo[2] = 0;

					switch (tast)
					{
						case 1:					
							ui_buffer_send[K_MOT]=50;		// Motorleistung = 50
							break;
						case 2:					
							ui_buffer_send[K_MOT]=100;		// Motorleistung = 100
							break;
						case 3:					
							ui_buffer_send[K_MOT]=200;		// Motorleistung = 200
							break;
						case 4:					
							richtung = 0;						// vorw�rts
							mo[3]= 0;
							break;
						case 5:					
							mo[3] = 0;						// Motor aus
							break;
						case 6:				
							richtung = 1; 
							mo[3]= 0;						// r�ckw�rts
							break;
						case 7:					
							if (richtung) 	mo[3]= -30;
							else  			mo[3]=  30;
							break;
						case 8:					
							if (richtung) 	mo[3]= -60;
							else  			mo[3]=  60;
							break;
						case 9:				
							if (richtung) 	mo[3]= -90;
							else  			mo[3]=  90;
							break;
					} 
					break;
				default: 
					menue[1]=1;
					break;
			}
			break;

		case 6:		//// Kickerplatine
			switch (menue[1]) 
			{
				// Kickerplatine
				// 1 = Kicken
				// 2 = Dribbler und Lichtschranke ein
				// 3 = Dribbler aus
				case 1:
					strcpy(zeile1,"6.1.KP      |Kick =1");
					strcpy(zeile2,"HP: D   K   |D ein=2");
					strcpy(zeile3,"KP: D   K   |D aus=3");

					printnbr(ui_buffer_recieve[DRIBB], 2, 5);
					printnbr(ui_buffer_recieve[KICK] , 2, 9);
					printnbr(ui_buffer_recieve[SDRIB], 3, 5);
					printnbr(ui_buffer_recieve[SKICK], 3, 9);	
														
					ui_buffer_send[KICK]=0b01;  				// Kicker zur�ckfahren
				
					switch (tast)
					{
						case 1:					
								ui_buffer_send[KICK]=0b11; 		// Kicken
				//				kicken = true;
								break;
						case 2:								
								ui_buffer_send[DRIBB]=0x01;		// Dribbler ein
								break;
						case 3:								
								ui_buffer_send[DRIBB]=0x00;		// Dribbler aus
								break;											
					}						
					break;

				case 2:
					strcpy(zeile1,"6.2.KP              ");
					strcpy(zeile2,"                    ");
					strcpy(zeile3,"                    ");
					break;
					
				default:
					menue[1]=1;
					break;
			}
			break;
		case 7:
			break;
		case 9: 
			break;
		case 8:		//Bluetooth
			switch (menue[1]) 
			{
				case 1:
					strcpy(zeile1,"Bluetooth           ");
					strcpy(zeile2,"Aktiv               ");
					strcpy(zeile3,"inc:               >");

					printnbr(UDR1, 3, 5);
					if(tast > 0 && tast < 10) UDR1 = tast;
					
					break;

				case 2:
					strcpy(zeile1,"Bluetooth           ");
					strcpy(zeile2,"5: setmaster        ");
					strcpy(zeile3,"m                   ");

					printnbr(bt_master, 3, 2);

					if(tast == 5 && tast != tastold) {
						bt_master ^= 1;
						set_master();
					}
			}
			break;					
			
	}

	// untere Zeile im Display
	strcpy(zeile4,"********************");
	// Spannung ausgeben
	itoa(ui_buffer_recieve[SPANNUNG],buffer,10);
	for(int i=0;i<10;i++)
	{
		if (buffer[i]==0) break;
		zeile4[i]=buffer[i];
	}
	// Kompassausgeben
	itoa(kompass_ist, buffer, 10);
	for(int i=0;i<10;i++)
	{
		if (buffer[i]==0) break;
		zeile4[i+4]=buffer[i];
	}
	printnbr(ball_richtung, 4, 8);
	// Fehlerindexausgeben
	if (ui_buffer_send[47]>1) error_anzahl++;
	printnbr(error_anzahl, 4, 12);
	printnbr(ui_buffer_send[47], 4, 15);
	ui_buffer_send[47] = 0;
	
	// Menuenummer ausgeben
	zeile4[18] = menue[0] + 48;
	zeile4[19] = menue[1] + 48;

	// Unterspannungsanzeige
	if(unterspannung)
	{ 
		strcpy(zeile1,"                    ");
		strcpy(zeile2,"     !Achtung!      ");
		strcpy(zeile3,"   !Unterspannung!  ");
	}


	if(zeitscheibe==1)
	{
		ausgabexy(1,1,zeile1);
	}

	else if(zeitscheibe==2)
	{
		ausgabexy(2,1,zeile2);
	}

	else if(zeitscheibe==3)
	{
		ausgabexy(3,1,zeile3);
	}

	else if(zeitscheibe==4)
	{
		ausgabexy(4,1,zeile4);
	}

	else{}

	zeitscheibe++;
	if(zeitscheibe > 11) zeitscheibe = 0;

	tastold = tast;

	return 0;
}

#endif
