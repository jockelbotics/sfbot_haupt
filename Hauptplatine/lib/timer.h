// Robo32 - timer.h

#ifndef TIMER_H
#define TIMER_H



void timer_init(void)
{
	TCCR0B |= (1<<CS02) | (1<<CS00);	// Vorteiler f�r 10ms Takt von 1024
	TIMSK0 |= (1<<TOIE0);				// TimerOverflowInterrupt aktivieren

	TCCR2B |= (1<<CS02) | (1<<CS01);	// Prescaler f�r 1ms
	TIMSK2 |= (1<<TOIE0) | (1<<OCIE0A); // compare A + overflow
	OCR2A = 186;

	zeit    = 0;
}

// Milisekundenpause

void msleep(int msec)
{
    _delay_ms(msec);
}


// Zeitinterruptroutine

SIGNAL (SIG_OVERFLOW0) 
{
	static uint8_t blinker=0;
	#ifdef HAUPTPLATINE
	// Starten der Kommunikation auf der Hauptplatine
	UDR0 = 255;	
	#endif

	TCNT0  = 61;		//Vorladen f�r 5ms Takt
	zeit++;
	blinker++;
	if(blinker==100)
	{
		led_toggle(ROT);
		blinker=0;
	}
}

SIGNAL (SIG_OVERFLOW2)
{
	TCNT2 = 178;	// 1ms Vorladen

	zeit_ms += 1;
}


SIGNAL (SIG_OUTPUT_COMPARE2A)
{
		// 100us
	
}
 
#endif
