// Robo32 - display_tastatur.h (29.03.2009)

#ifndef DISPLAY_TASTATUR_H
#define DISPLAY_TASTATUR_H

#include "i2cmaster.h"

// Ausgabe eines Steuerzeichen

void zeichen(int i)
{
	cli();
    i2c_init();
    if (i2c_start(Dev + I2C_WRITE)) return; 
    if (i2c_write(i)) return; 
    if (i2c_write(0x00)) return;                         
    i2c_stop();
	sei();
}

//Ausgabe einer Zeichenkette ab possition X/Y (Zeile(1-4)/Spalte(1-19))

void ausgabexy(uint8_t zeile, uint8_t spalte, char zeichen[])
{
	int i=0,z;
	cli();

	i2c_init();
    if (i2c_start(Dev + I2C_WRITE)) return;
	if (i2c_write(3)) 				return;
	if (i2c_write(zeile)) 			return;
	if (i2c_write(spalte)) 			return;
	while (1)
	{
		z = (int)zeichen[i];
		i++;
		if (i2c_write(z))	break;
		if (z == 0) 		break;
	}
    i2c_stop();
	sei();
} 

// Ausgabe einer Zeichenkette

void ausgabe(char zeichen[])
{
	int i=0,z;
	cli();

	i2c_init();
    if (i2c_start(Dev + I2C_WRITE)) return; 
	while (1)
	{
		z = (int)zeichen[i];
		i++;
		if (i2c_write(z)) return;
		if (z == 0) break;
	}
    i2c_stop();
	sei();
} 

// L�schen des Bildschirms

void cls(void)
{
	zeichen(12);
}

// L�schen einer Zeile

void cls_zeile(uint8_t zeile)
{
	ausgabexy(zeile,1,"                    ");
}

// L�schen eines Zeichen

void cls_zeichen(uint8_t y_,uint8_t x_)
{
	ausgabexy(y_,x_," ");
}

// Neue Zeile

void cr(void)
{
	zeichen(13);
}


// Auslesen eines Zeichens von der Tastatur

int8_t tastatur(void)
{
	char eingabe;
	cli();

	i2c_init();								
	if (i2c_start(Dev + I2C_READ)) return -2;
    eingabe = i2c_readNak();                     
    i2c_stop(); 
	sei();
		
	switch (eingabe)
    { 
        case 48:   return 0; 
        case 49:   return 1; 
        case 50:   return 2; 
        case 51:   return 3; 
        case 52:   return 4; 
        case 53:   return 5; 
        case 54:   return 6; 
        case 55:   return 7; 
        case 56:   return 8;
        case 57:   return 9; 		
	    case 42:   return 42; 	
	    case 35:   return 35; 
		default:   return -1;		
	}
}

int16_t zahleneingabe(uint8_t laenge,uint8_t y,uint8_t x,int16_t min,int16_t max)
{
    //////////////////////////////////////////////////
	// Erm�glicht eine Einfache Zahleneingabe 		//
	// per Tastatur                            		//
    // zaehleneingabe(3,2,1,100,300);                // 
	//----------------------------------------------//
	// Liefert die Eingabe zur�ck   				//
	//////////////////////////////////////////////////
    int16_t eingabe=0;
    uint8_t i=0,blinken=0,minus_zeiger=0, tast=0;
    char buffer[10];
    for(i=0;i<laenge;i++) 
	{
		ausgabexy(y,i+x,"_");
    	msleep(10);
	}
	for(i=0;i<laenge;i++)
    {
        do
        { // Warten bis Taste gedr�ckt wird
            if(blinken==1)  ausgabexy(y,i+x,"_"); 
            if(blinken==10) ausgabexy(y,i+x," ");
            if(blinken==20) blinken=0;
            msleep(50);
            blinken++;
            tast=tastatur();
        }
        while(tast==-1); 
        if(tast==35&&i==0) 
        {
            minus_zeiger=1;
            laenge++;
            ausgabexy(y,laenge+x-1,"_");
			msleep(10);
            ausgabexy(y,x,"-");
		}
        else if(tast==0&&eingabe==0)
        {
            cls_zeichen(y,laenge+x-1);
            laenge--;
            i--;
        }
        else
        {
            eingabe=eingabe*10+tast;
            itoa(tast,buffer,10);
            ausgabexy(y,i+x,buffer);
        }
        while(tastatur()!=-1) msleep(30); // Warten bis keine Taste gedr�ckt wird
    }
	if(minus_zeiger==1) eingabe=eingabe*-1;
    if(eingabe>max) eingabe=max;
    if(eingabe<min) eingabe=min;
	itoa(eingabe,buffer,10);
    ausgabexy(y,x,buffer);
    return eingabe;
    
}


// Ausgabe einer 3-stelligen Dezimalzahl

void izahl(int z)
{
	char text[5];
    int xe,xz,xh;
	if (z<0) 
	{
		z = -z;
		text[0] = '-';
	} 
	else 
	{
		text[0] = '+';
	}
	
	xh = z / 100;
	xz = (z-xh*100)/10;
	xe = z-xh*100-xz*10;
	
	text[1] = (char)(xh + '0');
	text[2] = (char)(xz + '0');
	text[3] = (char)(xe + '0'); 
	text[4] = 0;
    ausgabe(text);
}

#endif
