// Heiko Baumann - ATMega644P - main.c
//
//
//
//
//	Display Erkennung! DONE
//
//
//
//
/************************************************************/
#define BUS 1//////////////    CHECK    //////////////////////
/************************************************************/
#define REGLER_HINTEN 		-7		//unused
#define REGLER_SEITE		-2		//unused
#define DECAY_TIME			2000	// 2sec

#define MAX_MSPEED	100
#define RECHTS 1
#define LINKS  2
#define VORNE  3
#define HINTEN 4
#define DIP1 (dips & 1<<0)
#define DIP2 (dips & 1<<1)
#define DIP3 (dips & 1<<2)
#define DIP4 (dips & 1<<3)
#define DIP5 (dips & 1<<4)
#define DIP6 (dips & 1<<5)
#define TAST1 (taster & 1<<0)
#define TAST2 (taster & 1<<1)
#define TAST3 (taster & 1<<2)
#define TAST4 (taster & 1<<3)
#define SCHALT1 (schalter & SCHALTER1)
#define SCHALT2 (schalter & SCHALTER2)
#define SCHALT3 (schalter & SCHALTER3)
#define SCHALT4 (schalter & SCHALTER4)
#define MASK_RECHTS (0b00110000)
#define MASK_LINKS  (0b00000011)
#define MASK_HINTEN  (0b00001100)
#define MASK_VORNE (0b11000000)

#define DREHEN_KICK 		1
#define RESET 				0
#define ANSTOSS_1 			10
#define ANSTOSS_1_DAUER		100
//BT
#define HAS_BALL 0b01000000
#define STURMER  0b00000000
#define TORMANN  0b00000001
#define SENDEBIT 0b10000000
//zeiten
#define DELAY100ms 10
//ZAHLEN STIMMEN MIT IR ARRAY �BEREIN, NICHT MIT SCHALTPLAN
#define SENSOR_6_ANGLE -45
#define SENSOR_0_ANGLE 0
#define SENSOR_2_ANGLE 45
#define SENSOR_5_ANGLE -90
#define SENSOR_3_ANGLE 90
#define SENSOR_7_ANGLE -135
#define SENSOR_1_ANGLE 0
#define SENSOR_4_ANGLE 135

#define SENSOR_5_ANGLE_H 90
#define SENSOR_3_ANGLE_H -90
#define SENSOR_7_ANGLE_H 45
#define SENSOR_4_ANGLE_H -45
//
//	6 0 2
//	5   3
//	7 1	4

#include "global.h"


// Hauptprogramm




int main (void)
{

	uint8_t		i; //, kp=3, kd=1, i=0;
	uint8_t tmp = 0;

	int8_t mo_linie[4] = { 0 };
	uint8_t ball_linie =0;
//	uint8_t timer_seite = 0;
//	uint16_t line_decay = 0;
//	uint8_t timer_seite_evade = 0;


	uint8_t		error=0;// mot=0;
	
	bool motorstop = false;
	uint32_t tmp_time = 0;
	

	int8_t tordifferenz = 0;

	int16_t kick_timer = -1;	// loops zum beschleunigen vor kick
	bool turn360 = false;

	int8_t  taktik = 0;			//taktiken	
	bool anstoss = false;	
	int16_t anstoss_timer = -1;		//loops f�r bewegungsablauf
    
	
	// Initialisierung des Robo-Boards
    init();

	bt_master = eeprom_read_byte((uint8_t*)1);


	kompass_vorne = i2c_lego_kompass(); 
	kompass_vorne_start = kompass_vorne;
		
	while(1)
	{	
		//EVA Prinzip
		if(bus_intern_flags == 0b1		||		!BUS)
		{	
			bus_intern_flags = 0;

			tmp_time = zeit_ms;

	//		if(display) PORTD |= (1<<4);
			//////////////////////////////////
			//Eingabe auslesen der einzelnen Eing�nge
			//////////////////////////////////

			update_sensors();

			if (irtast & (1<<0)) motorstop = false;
			if (diptast & (1<<0) /*&& ui_buffer_recieve[SKICK] == 1*/) kick_timer = 25;// kicken = true;
	//		if (diptast & (1<<1) /*&& ui_buffer_recieve[SKICK] == 1*/) ui_buffer_send[KICK] = 0b11;// kicken = true;
			if (diptast & (1<<2))rambo = true;

			else rambo = false;
			//DIP TAKTIKEN
			if(!anstoss) {
				if(DIP6) taktik = DREHEN_KICK;		//nach ballfangen drehen und kicken
				else taktik = 0;
			}
			// ANSTOSS TAKTIKEN ZUM �BERSCHREIBEN / NACH ERSTEM ZYKLUS NICHT MEHR VON BEDEUTUNG DA VON TIMER ABH�NGIG			
			if(TAST2) {
				taktik = ANSTOSS_1;
				anstoss = true;
				anstoss_timer = ANSTOSS_1_DAUER;
			}
/****************** SENSORPLATINEN WINKEL *****************************************/
			ball_richtung = (ui_buffer_recieve[BALLWINKEL]); 
			if(ui_buffer_recieve[BALLENTFERNUNG] & 0b10000000) ball_richtung = -ball_richtung;

			ball_entfernung = ui_buffer_recieve[BALLENTFERNUNG] & ~(1<<7);
/******************** HAUPTPLATINEN WINKEL ***************************************/
			//�bertragung von fern winkel
//			if(ball_richtung == -200) ball_richtung = fern_winkel;		//wenn sensorplatine nichts sieht
			if(ball_entfernung == 0)  ball_entfernung = fern_abstand;
/******************** Absoluter Winkel zum Spielfeld *****************************/
			ball_richtung_absolut = ball_richtung + (kompass_roh - kompass_vorne_start);
			///////////////////////////////////////////
			// Verarbeitung der aufgenommenen Sensorwerte
			///////////////////////////////////////////
			if(rambo) {
				kompass_vorne += 180;
				if(kompass_vorne<(-180)) 	kompass_vorne += 360;
				if(kompass_vorne>180)		kompass_vorne -= 360;
				if(ball_richtung > -200) {
					ball_richtung += 180;
					if(ball_richtung < -180) 	ball_richtung += 360;
					if(ball_richtung > 180)		ball_richtung -= 360;
				}
			}
			else kompass_vorne = kompass_vorne_start;

			kompass_alt		=	kompass_ist;

			kompass_ist = get_kompass_ist();
/*
			kompass_ist		=	kompass_roh-kompass_vorne;
			
			if(kompass_ist<(-180)) 	kompass_ist += 360;
			if(kompass_ist>180)		kompass_ist -= 360;
*/

			if(kompass_ist > -2 && kompass_ist < 2) kompass_ist = 0;
		


			
/////////////////////////////////////
			//ausrichten auf tor, finale nutzung: kompass_vorne += tordifferenz;
			tordifferenz = (sharps.rechts - sharps.links)*2;
			if(sharps.vorne > abs(tordifferenz)) tordifferenz = 0;
			else tordifferenz -= sharps.vorne;

///////////////////////////////
//		BT
///////////////////////////////
//		SLAVE BYTE
//
//			0b  1       0       1 0 1 0 1 0
//				|		|		|		  |
//		immer gesetzt	|		|		  |
//					hat ball	|		  |
//							   ball richtung
//												KONVERTIERUNG IN WENIGE F�LLE
//
//		MASTER BYTE
//
//			0b  1    0 1 0 1 0 1   0
//				|				   |
//		immer gesetzt			   |
//							tormann/st�rmer

			tormann = false;
			bt_last = UDR1;
			if(bt_last) {
				if(bt_master) {		//MASTER				fehlende logik f�r position zum ball
					if(bt_last & HAS_BALL) {
						tormann = true;					//werde tormann
						UDR1 = SENDEBIT + STURMER;		//send st�rmer
					}
					else if(ui_buffer_recieve[SDRIB]) {
						UDR1 = SENDEBIT + TORMANN;		//send tormann
						tormann = false;				//werde st�rmer
					}
					else { //beide st�rmer
						tormann = false;				//werde st�rmer
						UDR1 = SENDEBIT + STURMER;		//send st�rmer
					}
				}
				/******/
				else {		//SLAVE
					tmp = SENDEBIT;		//build outgoing byte / reset
					if(ui_buffer_recieve[SDRIB]) {
						tmp |= HAS_BALL;		//slave sendet ball status
						tormann = false;
					}						//180/3=60		max: 64 bei 6bit
					tmp |= (abs(ball_richtung) / 3);  // << 5 ;

					UDR1 = tmp;		//SEND

					//befehle von master verarbeiten
					if(bt_last & TORMANN) tormann = true;
					else tormann = false;
				}
			}



	//////////////////////
	//		VERHALTEN
	//////////////////////
			
/*
			//ballfolgen
			if(ball_richtung == -200) regler_k(0, 0);
			else if(ball_richtung > -3 && ball_richtung < 3) regler_k(0, 50);
			else if(ball_richtung >-30 && ball_richtung < 30) regler_k(ball_richtung, 0);	
			else if(abs(ball_richtung) > 111) regler_k(-(ball_richtung/10), -(abs(ball_richtung)/8));
			else if(abs(ball_richtung > 60)) regler_k(ball_richtung/8, -35);
		 	else regler_k(ball_richtung*0.7, -40) ;
*/			
			if(ball_richtung == -200) regler_k(0,0);
			
	//		regler_k(0, 0);

			
				
				
				
			if(taktik == 1) {
				regler_b();
				
				if(ui_buffer_recieve[SDRIB]) {
					if(kompass_ist > -5 && kompass_ist < 5) kick_timer = 18;
					else {
						if(kompass_ist < 0)	for(i=0;i<4;i++) mo[i] = -5;
						else if(kompass_ist > 0) for(i=0;i<4;i++) mo[i] = 5;
					}
				}
			}
			else if(taktik == ANSTOSS_1) {
				if(anstoss_timer == ANSTOSS_1_DAUER) for(i=0;i<4;i++) mo[i] = 0;		//warten auf start
				else if(anstoss_timer == 0)	{
					kick_timer = 18;
					anstoss_timer = -1;						//
					anstoss = false;
				}
				else if(anstoss_timer > 10 && kompass_vorne == kompass_vorne_start) modify_cv(+35);
				else kompass_vorne = kompass_vorne_start;
				regler_k(0,35);
				anstoss_timer--;
			} 
			else {

				//ballfolgen
				if(ui_buffer_recieve[SDRIB] && kompass_ist >= -4 && kompass_ist <= 4&& kick_timer < 0) kick_timer = 18;
		
		
				if(ball_richtung == -200) {	
			//		regler_k((sharps.rechts - sharps.links)*REGLER_SEITE, (sharps.hinten-10)*REGLER_HINTEN);
					regler_k(0,-25);
				}
/*
				else if(ball_richtung >= -1 && ball_richtung <= 1) regler_k(0, 75);				//0
				else if(ball_richtung >= -5 && ball_richtung <= -2) regler_k(-5, 35);
				else if(ball_richtung >= 2 && ball_richtung <= 5) regler_k(5, 35);
				else if(ball_richtung >= -15 && ball_richtung <= -6) regler_k(-10, 35);
				else if(ball_richtung >= 6 && ball_richtung <= 15) regler_k(10, 35);
				else if(ball_richtung >= -25 && ball_richtung <= -16) regler_k(-15, 30);
				else if(ball_richtung >= 16 && ball_richtung <= 25) regler_k(15, 30);
				else if(ball_richtung >= -35 && ball_richtung <= -26) regler_k(-20, 20);
				else if(ball_richtung >= 26 && ball_richtung <= 35) regler_k(20, 20);
				else if(ball_richtung >= -45 && ball_richtung <= -36) regler_k(-25, 20);
				else if(ball_richtung >= 36 && ball_richtung <= 45) regler_k(25, 20);
				else if(ball_richtung >= -55 && ball_richtung <= -46) regler_k(-30, 20);
				else if(ball_richtung >= 46 && ball_richtung <= 55) regler_k(30, 20);
				else if(ball_richtung >= -65 && ball_richtung <= -56) regler_k(-30, 10);
				else if(ball_richtung >= 56 && ball_richtung <= 65) regler_k(30, 10);
				else if(ball_richtung >= -75 && ball_richtung <= -66) regler_k(-35, 5);
				else if(ball_richtung >= 66 && ball_richtung <= 75) regler_k(35, 5);
				else if(ball_richtung >= -85 && ball_richtung <= -76) regler_k(-35, 0);
				else if(ball_richtung >= 76 && ball_richtung <= 85) regler_k(35, 0);
				else if(ball_richtung >= -95 && ball_richtung <= -86) regler_k(-30, -5);
				else if(ball_richtung >= 86 && ball_richtung <= 95) regler_k(30, -5);
				else if(ball_richtung >= -105 && ball_richtung <= -96) regler_k(-35, -10);
				else if(ball_richtung >= 96 && ball_richtung <= 105) regler_k(35, -10);
				else if(ball_richtung >= -115 && ball_richtung <= -106) regler_k(-30, -15);
				else if(ball_richtung >= 106 && ball_richtung <= 115) regler_k(30, -15);
				else if(ball_richtung >= -125 && ball_richtung <= -116) regler_k(-30, -15);
				else if(ball_richtung >= 116 && ball_richtung <= 125) regler_k(30, -15);
				else if(ball_richtung >= -135 && ball_richtung <= -126) regler_k(-25, -20);
				else if(ball_richtung >= 126 && ball_richtung <= 135) regler_k(25, -20);
				else if(ball_richtung >= -145 && ball_richtung <= -136) regler_k(-20, -20);
				else if(ball_richtung >= 136 && ball_richtung <= 136) regler_k(20, 75);
				else if(ball_richtung >= -155 && ball_richtung <= -146) regler_k(-15, -25);
				else if(ball_richtung >= 146 && ball_richtung <= 155) regler_k(15, -25);
				else if(ball_richtung >= -165 && ball_richtung <= -156) regler_k(-10, -25);
				else if(ball_richtung >= 156 && ball_richtung <= 165) regler_k(10, -25);
				else if(ball_richtung >= -175 && ball_richtung <= -166) regler_k(-10, -30);
				else if(ball_richtung >= 166 && ball_richtung <= 180) regler_k(5, -40);
				else if(ball_richtung >= -180 && ball_richtung <= -176) regler_k(-5, -40);
			
*/			


				else if(ball_richtung > -3 && ball_richtung < 3) regler_k(0, 50);
				else if(ball_richtung >-30 && ball_richtung < 30) regler_k(ball_richtung*2, 0);	
				else if(abs(ball_richtung) > 111) regler_k(-(ball_richtung/8), -(abs(ball_richtung)/8));
				else if(abs(ball_richtung > 60)) regler_k(ball_richtung/6, -40);
			 	else regler_k(ball_richtung*0.7, -40) ;			
		
			}	
															
			//KICK
			if(kick_timer > 0) {
				regler_k(0,55);
				if(kick_timer <15) ui_buffer_send[DRIBB] = 0x00;	//dribbler aus
				kick_timer--;
				ui_buffer_send[KICK]=0b01;						//kicker zur�ckziehen
			}
			if(kick_timer == 0)
			{
				ui_buffer_send[KICK] = 0b11;		//exec kick
				ui_buffer_send[DRIBB] = 0x01;		//dribbler ein
				for(i=0;i<4;i++) mo[i] = 0;
				kick_timer = -1;
		//		motorstop = true;
			}
			if(kick_timer > 8 && sharps.vorne < 25 && sharps.vorne > 0) turn360 = true;
			if(turn360) {
			if(ui_buffer_recieve[SDRIB] == 0) turn360 = false;
				ui_buffer_send[K_MOT]=50;
				for(i=0;i<4;i++) mo[i] = 5;
				ui_buffer_send[DRIBB] = 0x01;
				if(kick_timer < 15) kick_timer = -1;	//force spin
				if(kompass_ist > -8 && kompass_ist < 8 && kick_timer == -1) {
					kick_timer = 18;
					turn360 = false;
					ui_buffer_send[K_MOT]=100;
				}
			}


	//		if(ui_buffer_recieve[SDRIB]) motorstop = true;

			//LINIEN
				
//			BUS BYTE SORTIERUNG:
//				vorne
//				bit 7-6
//		bit 1-0			bit 5-4			�u�ere immer zuerst (von links im byte)
//				bit 3-2
				
			if	   (ui_buffer_recieve[BODENSENSOREN]) motorstop = true;
/*			switch(fsm_linie) {
				case 0: 
					if(ui_buffer_recieve[BODENSENSOREN]) {
	//			motorstop = true;
						if	   (ui_buffer_recieve[BODENSENSOREN] & 0b00110000) seite = RECHTS;
						else if(ui_buffer_recieve[BODENSENSOREN] & 0b00000011) seite = LINKS;
						else if(ui_buffer_recieve[BODENSENSOREN] & 0b11000000) seite = VORNE;
						else if(ui_buffer_recieve[BODENSENSOREN] & 0b00001100) seite = HINTEN;
						fsm_linie = 1;
//				if(!line_decay) line_decay = DECAY_TIME;
					}
					else
					{
						fsm_linie=0;
						seite = 0;
					}
					break;
				case 1:
					if(ui_buffer_recieve[BODENSENSOREN]) fsm_linie = 2;
					if(seite == RECHTS && (ui_buffer_recieve[BODENSENSOREN] & MASK_RECHTS)) fsm_linie = 0;
					else if(seite == LINKS && (ui_buffer_recieve[BODENSENSOREN] & MASK_LINKS)) fsm_linie = 0;
					else if(seite == VORNE && (ui_buffer_recieve[BODENSENSOREN] & MASK_VORNE)) fsm_linie = 0;
					else if(seite == HINTEN && (ui_buffer_recieve[BODENSENSOREN] & MASK_HINTEN)) fsm_linie = 0;
					else if(!ui_buffer_recieve[BODENSENSOREN]) fsm_linie = 1;
					
					break;

				case 2:

					switch(seite) {

						case RECHTS: regler_k(-30, 0); break;
						case LINKS: regler_k(30, 0);  break;
						case VORNE: regler_k(0, -20); break;
						case HINTEN: regler_k(0, 20); break;
					}
					
					if(seite == RECHTS && (ui_buffer_recieve[BODENSENSOREN] & MASK_RECHTS)) fsm_linie = 0;
					else if(seite == LINKS && (ui_buffer_recieve[BODENSENSOREN] & MASK_LINKS)) fsm_linie = 0;
					else if(seite == VORNE && (ui_buffer_recieve[BODENSENSOREN] & MASK_VORNE)) fsm_linie = 0;
					else if(seite == HINTEN && (ui_buffer_recieve[BODENSENSOREN] & MASK_HINTEN)) fsm_linie = 0;

					break;
				default:
					fsm_linie = 0;
					break;
			
			}
*/

			if(DIP2) regler_k(0,0);
	/////////////////////
	//		AUSGABE
	/////////////////////		
			
			if(display)	display_ausgabe();

				

			
			
			
			if(motor_schalter || motorstop) //Testmodus zur Ausgabe von Werten
			{	
				if(motor_schalter) {
					ui_buffer_send[V_MOT0]=0;
					ui_buffer_send[V_MOT1]=0;
					ui_buffer_send[V_MOT2]=0;
					ui_buffer_send[V_MOT3]=0;
				}
/*				else {
		//			if(!mo_linie[0]) {		//nur setzen wenn erster zyklus
						for(i=0;i<4;i++) mo_linie[i] = -ui_buffer_recieve[VIMOT0+i*2]/5;
						for(i=0;i<4;i++) {	//begrenzen
							if(mo_linie[i]>MAX_MSPEED) mo_linie[i] = MAX_MSPEED;
							if(mo_linie[i]<-MAX_MSPEED) mo_linie[i] = -MAX_MSPEED;
							if(mo_linie[i]<0) mo_linie[i]=abs(mo_linie[i])| 0b10000000;

		//				}
					}		// umdrehen wenn ungleich null
	//				mo_linie[0] = -(ui_buffer_recieve[VIMOT0]);
	

						//ALTERNATIV: IMMER ISTGESCHWINDIGKEIT UMDREHEN
					if(ui_buffer_recieve[VIMOT0] > 0) ui_buffer_send[V_MOT0]=mo_linie[0];
					else ui_buffer_send[V_MOT0]= 0;
					if(ui_buffer_recieve[VIMOT1] > 0) ui_buffer_send[V_MOT1]=mo_linie[1];
					else ui_buffer_send[V_MOT1]= 0;
					if(ui_buffer_recieve[VIMOT2] > 0) ui_buffer_send[V_MOT2]=mo_linie[2];
					else ui_buffer_send[V_MOT2]= 0;
					if(ui_buffer_recieve[VIMOT3] != 0) ui_buffer_send[V_MOT3]=mo_linie[3];
					else ui_buffer_send[V_MOT3]= 0;
	
	
					
					if(DIP5) for(i=0;i<4;i++) ui_buffer_send[V_MOT0 +i] = 128;
					else for(i=0;i<4;i++) ui_buffer_send[V_MOT0 +i] = 0;
//					
//					if(ball_richtung < 0 && ball_linie == 2){ motorstop = false;	timer_seite = 25; }
//					else if(ball_richtung > 0 && ball_linie == 1){ motorstop = false;	timer_seite = 25; }
					if(false) {
			//			if(!timer_seite_evade) motorstop = false;
						if(seite == LINKS) regler_k(30, 0);
						else if(seite == RECHTS) regler_k(-30, 0);
						else if(seite == VORNE) regler_k (0, -30);
						else if(seite == HINTEN) regler_k(0, 15);
						
					}

				}
*/
			}
			else
			{
				for(i=0;i<4;i++) mo_linie[i] = 0;
				//ausgabe_zeiger=0;
				for(i=0;i<4;i++)
				{
					if(mo[i]>MAX_MSPEED) mo[i] = MAX_MSPEED;
					if(mo[i]<-MAX_MSPEED) mo[i] = -MAX_MSPEED;
					if(mo[i]<0) mo[i]=abs(mo[i])| 0b10000000;

				}

				ui_buffer_send[V_MOT0]=mo[0];
				ui_buffer_send[V_MOT1]=mo[1];
				ui_buffer_send[V_MOT2]=mo[2];
				ui_buffer_send[V_MOT3]=mo[3];
			}

			if(ui_buffer_recieve[SPANNUNG]<130	&&	BUS) unterspannung = true;
			else unterspannung = false;
			if(unterspannung) led_ein(ROT);

			if(bus_intern_flags == 0b1) error++;

			time_remaining = 10 - (zeit_ms - tmp_time);
		}
		
	}

    // Ende eigener Code
    while(1);

    return 1;
} 
