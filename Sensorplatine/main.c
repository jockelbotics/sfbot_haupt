//ATMega644P - main.c

/************************************************************/
#define BUS 1//////////////    CHECK    //////////////////////
/************************************************************/
#define DELAY100ms 10

//vorne
#define SENSOR_7_ANGLE 		45
#define SENSOR_8_ANGLE 		22	//,5
#define SENSOR_24_ANGLE 	0
#define SENSOR_23_ANGLE 	0
#define SENSOR_9_ANGLE 		-22
#define SENSOR_10_ANGLE 	-45
//fein
#define SENSOR_20_ANGLE 	-127
#define SENSOR_12_ANGLE 	-76
#define SENSOR_11_ANGLE 	-25
#define SENSOR_5_ANGLE 		25
#define SENSOR_15_ANGLE 	76
#define SENSOR_4_ANGLE 		127
//links
#define SENSOR_22_ANGLE 	-22
#define SENSOR_21_ANGLE 	-67
#define SENSOR_13_ANGLE 	-112
#define SENSOR_14_ANGLE 	-157
//rechts
#define SENSOR_6_ANGLE 		22
#define SENSOR_17_ANGLE 	67
#define SENSOR_19_ANGLE 	112
#define SENSOR_18_ANGLE 	157
//hinten
#define SENSOR_3_ANGLE 		-45
#define SENSOR_0_ANGLE 		-22
#define SENSOR_1_ANGLE 		0
#define SENSOR_2_ANGLE 		22
#define SENSOR_16_ANGLE 	45
//links hinten
#define SENSOR_14_ANGLE_H   22
#define SENSOR_13_ANGLE_H   67
#define SENSOR_21_ANGLE_H   112
//rechts hinten
#define SENSOR_18_ANGLE_H   -22
#define SENSOR_19_ANGLE_H   -67
#define SENSOR_17_ANGLE_H   -112

#define PULS_AUS			20		// 1 ms
#define PULS_AN				24		// 40 = 2ms
#define PULS_SL				22

#include "global.h"
// Hauptprogramm


int main (void)
{
    // Initialisierung des Robo-Boards
    init();

	//Variablen
	char buffer[10];

//	uint8_t ir[25];

	
//	uint8_t ausgabe_counter=0;
	uint8_t i=0, i2=0;

	
//	while(1);	
//
/*			   24?
*		 07 08 -- 23 09 10 
*
*		 20 12 11 05 15 04
*
*	  22				   06
*
*	 21				        17
*
*	 13			    	    19
*
*	  14		    	   18
*
*	      03 00 01 02 16   
*/ 

	while(1)
	{	
		if(bus_intern_flags == 0b1		||		!BUS)
		{
//			if(display)	led_ein(ROT);
			//Eingabe
/*************** irs ***********************/
			//reset
			memset(ir_vorne, 0, sizeof(ir_vorne));	
			memset(ir_vorne_fein, 0, sizeof(ir_vorne_fein));
			memset(ir_hinten, 0, sizeof(ir_hinten));
			memset(ir_links, 0, sizeof(ir_links));
			memset(ir_rechts, 0, sizeof(ir_rechts));

			for(i2=0;i2<10;i2++) {


				multiplexer_ir_abfragen[0] 	= multiplexer_ir_abfragen_1();
				multiplexer_ir_abfragen[1] 	= multiplexer_ir_abfragen_2();	
				ir_abfragen[0]			   	= ir_abfragen_1();
				ir_abfragen[1]			   	= ir_abfragen_2();


				if(multiplexer_ir_abfragen[0] & 1<<0) ir_hinten[1]++;
				if(multiplexer_ir_abfragen[0] & 1<<1) ir_hinten[2]++;
				if(multiplexer_ir_abfragen[0] & 1<<2) ir_hinten[3]++;
				if(multiplexer_ir_abfragen[0] & 1<<3) ir_hinten[0]++;
				if(multiplexer_ir_abfragen[0] & 1<<4) ir_hinten[4]++;
				if(multiplexer_ir_abfragen[0] & 1<<6) ir_rechts[3]++;
				if(multiplexer_ir_abfragen[0] & 1<<7) ir_rechts[2]++;


				if(multiplexer_ir_abfragen[1] & 1<<6) ir_vorne[3]++;
				if(multiplexer_ir_abfragen[1] & 1<<0) ir_rechts[0]++;
				if(multiplexer_ir_abfragen[1] & 1<<5) ir_rechts[1]++;
				if(multiplexer_ir_abfragen[1] & 1<<3) ir_vorne[4]++;
				if(multiplexer_ir_abfragen[1] & 1<<4) ir_vorne[5]++;
				if(multiplexer_ir_abfragen[1] & 1<<7) ir_vorne[2]++;		// ?? 24
				if(multiplexer_ir_abfragen[1] & 1<<2) ir_vorne[1]++;
				if(multiplexer_ir_abfragen[1] & 1<<1) ir_vorne[0]++;

				//prozessor, alle fein/im dribbler
				if(ir_abfragen[0] & 1<<0) ir_vorne_fein[5]++;
				if(ir_abfragen[0] & 1<<1) ir_vorne_fein[3]++;
				if(ir_abfragen[0] & 1<<2) ir_vorne_fein[4]++;
				if(ir_abfragen[0] & 1<<3) ir_vorne_fein[2]++;
				if(ir_abfragen[0] & 1<<4) ir_vorne_fein[1]++;
				if(ir_abfragen[0] & 1<<5) ir_vorne_fein[0]++;

				//prozessor, alle links
				if(ir_abfragen[1] & 1<<0) ir_links[0]++;	//22
				if(ir_abfragen[1] & 1<<1) ir_links[1]++;	//21
				if(ir_abfragen[1] & 1<<2) ir_links[2]++;	//13
				if(ir_abfragen[1] & 1<<3) ir_links[3]++;	//14

				_delay_us(90);

			}
/***********************************************/
			dips						= dips_auslesen();

/******************* Tasks *************************/
			ir_winkel = 0; //reset
			nah_bereich = false;

			ir_abstand = ir_vorne[0]+ir_vorne[1]+ir_vorne[2]+ir_vorne[3]+ir_vorne[4]+ir_vorne[5] +
						 ir_links[0]+ir_links[1]+ir_links[2]+ir_links[3]+
						 ir_rechts[0]+ir_rechts[1]+ir_rechts[2]+ir_rechts[3]+
						 ir_hinten[0]+ir_hinten[1]+ir_hinten[2]+ir_hinten[3]+ir_hinten[4];

			//fein sensoren erfassen Ball
			if((ir_vorne_fein[0] + ir_vorne_fein[1] + ir_vorne_fein[2] + 
			    ir_vorne_fein[3] + ir_vorne_fein[4] + ir_vorne_fein[5]) > 3) {
				
				nah_bereich = true;
				ir_winkel = 
				(
				 ir_vorne_fein[0]*SENSOR_20_ANGLE + ir_vorne_fein[1]*SENSOR_12_ANGLE + 
				 ir_vorne_fein[2]*SENSOR_11_ANGLE + ir_vorne_fein[3]*SENSOR_5_ANGLE  +
				 ir_vorne_fein[4]*SENSOR_15_ANGLE + ir_vorne_fein[5]*SENSOR_4_ANGLE
				)
				//geteilt durch summe
				/	((
					 ir_vorne_fein[0] + ir_vorne_fein[1] + ir_vorne_fein[2] + 
					 ir_vorne_fein[3] + ir_vorne_fein[4] + ir_vorne_fein[5] 
					)*25);
				//werte minimieren
				//ir_winkel /= 25;  
			}
			//ball wird nicht gesehen / ggf. Toleranz anpassen
			else if((ir_vorne[3]+ir_rechts[0]+ir_rechts[1]+ir_rechts[2]+ir_rechts[3]+ir_hinten[1]+ir_links[3]+ir_links[2]+ir_links[1]+ir_links[0]) < 2)
			{
				ir_abstand= 0;
				ir_winkel = -200;
			}
			//Ball hinter Bot
			else if((ir_hinten[0]+ir_hinten[1] + ir_hinten[2]+ir_hinten[3] + ir_hinten[4]) > 3)
			{
				ir_winkel =			
				(
/*links*/		 ir_links[1]*SENSOR_21_ANGLE_H + ir_links[2]*SENSOR_13_ANGLE_H + ir_links[3]*SENSOR_14_ANGLE_H +				
/*rechts*/		 ir_rechts[1]*SENSOR_17_ANGLE_H + ir_rechts[2]*SENSOR_19_ANGLE_H + ir_rechts[3]*SENSOR_18_ANGLE_H +				
/*hinten*/		 ir_hinten[0]*SENSOR_3_ANGLE + ir_hinten[1]*SENSOR_0_ANGLE + ir_hinten[2]*SENSOR_1_ANGLE + ir_hinten[3]*SENSOR_2_ANGLE + ir_hinten[3]*SENSOR_16_ANGLE
				)			
				//geteilt durch summe
				/	(
					 ir_links[1]+ir_links[2]+ir_links[3] +
					 ir_rechts[1]+ir_rechts[2]+ir_rechts[3] +
					 ir_hinten[0]+ir_hinten[1]+ir_hinten[2]+ir_hinten[3]+ir_hinten[4]
					);

				//Wenn der Winkel kleiner 0 ist werden 180 dazugez�hlt
				if(ir_winkel<0)	ir_winkel += 180;
				//Wenn er gr��er ist werden 180 abgezogen
				else			ir_winkel -= 180;
			}
			else {	//Ball vor Bot
				
				ir_winkel =			
				(
/*links*/		 ir_links[0]*SENSOR_22_ANGLE + ir_links[1]*SENSOR_21_ANGLE + ir_links[2]*SENSOR_13_ANGLE + ir_links[3]*SENSOR_14_ANGLE +
				
/*rechts*/		 ir_rechts[0]*SENSOR_6_ANGLE + ir_rechts[1]*SENSOR_17_ANGLE + ir_rechts[2]*SENSOR_19_ANGLE + ir_rechts[3]*SENSOR_18_ANGLE +
				
/*vorne*/		 ir_vorne[0]*SENSOR_7_ANGLE + ir_vorne[1]*SENSOR_8_ANGLE + ir_vorne[3]*SENSOR_23_ANGLE + ir_vorne[4]*SENSOR_9_ANGLE + ir_vorne[5]*SENSOR_10_ANGLE
				)			
				//geteilt durch summe
				/	(
					 ir_links[0]+ir_links[1]+ir_links[2]+ir_links[3] +
					 ir_rechts[0]+ir_rechts[1]+ir_rechts[2]+ir_rechts[3] +
					 ir_vorne[0]+ir_vorne[1]+ir_vorne[3]+ir_vorne[4]+ir_vorne[5]
					);


			}
			







/************************ Ausgabe **************************/
			if(ir_abstand > 100) ir_abstand = 100;

//			ir_winkel = -55;
//			ir_abstand = 99;

			
			//Vorzeichen von ir_winkel (16bit MSB) auf bus MSB zusammen mit entfernung �bertragen
			temp_ent					 		= ((ir_winkel & 0b1000000000000000) >> 8) | ir_abstand;	
			ui_buffer_send[BALLENTFERNUNG]		= temp_ent;
			ui_buffer_send[BALLWINKEL] 			= ((uint8_t)(abs(ir_winkel)));
/*****************************************************/


			if(dips & 0b1) display_ausgabe();


			if(ui_buffer_recieve[SPANNUNG]<130	&&	BUS) unterspannung = true;

			bus_intern_flags = 0;

		}

	}

    // Ende eigener Code
    while(1);

    return 1;
} 





/*
			
			for(i=0;i<100;i++)
			{
				//Eingabe
				multiplexer_ir_abfragen[0] 	= multiplexer_ir_abfragen_1();
				multiplexer_ir_abfragen[1] 	= multiplexer_ir_abfragen_2();	
				ir_abfragen[0]			   	= ir_abfragen_1();
				ir_abfragen[1]			   	= ir_abfragen_2();
				dips						= dips_auslesen();
						
				//Verarbeitung
			
				//Multiplexer 1
		
				if(multiplexer_ir_abfragen[0]	& 0b00000001)	ir[0]++;
				if(multiplexer_ir_abfragen[0]	& 0b00000010)	ir[1]++;	//Grad 125
				if(multiplexer_ir_abfragen[0]	& 0b00000100)	ir[2]++;
				if(multiplexer_ir_abfragen[0]	& 0b00001000)  	ir[3]++;
				if(multiplexer_ir_abfragen[0]	& 0b00010000)	ir[16]++;
				if(multiplexer_ir_abfragen[0]	& 0b01000000)	ir[18]++;	//Grad 109
				if(multiplexer_ir_abfragen[0]	& 0b10000000)	ir[19]++;	//Grad 78

				//Multiplexer 2
				if(multiplexer_ir_abfragen[1]	& 0b00000001)	ir[6]++;	//Grad 15
				if(multiplexer_ir_abfragen[1]	& 0b00000010)	ir[7]++;
				if(multiplexer_ir_abfragen[1]	& 0b00000100)	ir[8]++;
				if(multiplexer_ir_abfragen[1]	& 0b00001000)	ir[9]++;
				if(multiplexer_ir_abfragen[1]	& 0b00010000)	ir[10]++;
				if(multiplexer_ir_abfragen[1]	& 0b00100000)	ir[17]++;	//Grad 47
				if(multiplexer_ir_abfragen[1]	& 0b01000000)	ir[23]++;	//Grad 1
				if(multiplexer_ir_abfragen[1]	& 0b10000000)	ir[24]++;

				//IR abfragen 1
				if(ir_abfragen[0] 				& 0b00000001)	ir[4]++;
				if(ir_abfragen[0]				& 0b00000010)	ir[5]++;	
				if(ir_abfragen[0] 				& 0b00000100)	ir[15]++;
				if(ir_abfragen[0]				& 0b00001000)	ir[11]++;	
				if(ir_abfragen[0]				& 0b00010000)	ir[12]++;
				if(ir_abfragen[0]				& 0b00100000)	ir[20]++;
			
				//IR abfragen 2
				if(ir_abfragen[1]				& 0b00000001)	ir[22]++;  	//Grad 235
				if(ir_abfragen[1] 				& 0b00000010)	ir[21]++;	//Grad 203
				if(ir_abfragen[1]				& 0b00000100)	ir[13]++;	//Grad 172
				if(ir_abfragen[1]				& 0b00001000)	ir[14]++;	//Grad 141
			}
					

				//IR Winkel berechnen
				//Wenn alle keinen Ball sehen wird der IR-Winkel auf -1 gesetzt
				if((ir[23]+ir[6]+ir[17]+ir[19]+ir[18]+ir[0]+ir[14]+ir[13]+ir[21]+ir[22])==0)	ir_abstand=0;
				//Ber�cksichtigen des �berschlags falls der Ball hinter dem Bot liegt
				else if(((ir[14]+ir[13])>0)&&((ir[0]+ir[18])>0))
				{
					ir_abstand=1;
					//Alle Winkel einmal um 180 Grad drehen
					ir_winkel = ((ir[0]*0+ir[14]*15+ir[13]*47+ir[21]*78+ir[22]*109+ir[23]*125+ir[6]*(-109)+ir[17]*(-78)+ir[19]*(-47)+ir[18]*(-15))/(ir[23]+ir[6]+ir[17]+ir[19]+ir[18]+ir[0]+ir[14]+ir[13]+ir[21]+ir[22]));
					//Wenn der Winkel kleiner 0 ist werden 180 dazugez�hlt
					if(ir_winkel<0)	ir_winkel = 125+ir_winkel;
					//Wenn er gr��er ist werden 180 abgezogen
					else			ir_winkel = -125+ir_winkel;
				}
				//Normaler IR-Winkel, sofern kein �berschlag vorhanden ist und der Ball in Sicht ist			
				else
				{
				  ir_abstand = 1;
				  ir_winkel = ((ir[0]*125+ir[14]*(-109)+ir[13]*(-78)+ir[21]*(-47)+ir[22]*(-15)+ir[23]*1+ir[6]*15+ir[17]*47+ir[19]*78+ir[18]*109)/(ir[23]+ir[6]+ir[17]+ir[19]+ir[18]+ir[0]+ir[14]+ir[13]+ir[21]+ir[22]));
				}
			
				if(ir_winkel > 125) ir_winkel=125;
				if(ir_winkel <-125) ir_winkel=-125;
				ir_winkel +=125;
				ui_buffer_send[BALLWINKEL]= ir_winkel;
				ui_buffer_send[BALLENTFERNUNG] = ir_abstand;
			//Ausgabe
	
	//		if(dips & 0b00000001)
			{			
				if(ausgabe_counter==50)
				{			
					cls();
					itoa(ir[22],buffer,10);
					ausgabexy(1,1,buffer);
					itoa(ir[23],buffer,10);
					ausgabexy(1,5,buffer);
					itoa(ir[6],buffer,10);
					ausgabexy(1,10,buffer);
					itoa(ir[21],buffer,10);
					ausgabexy(2,1,buffer);
					itoa(ir[17],buffer,10);
					ausgabexy(2,10,buffer);
					itoa(ir[13],buffer,10);
					ausgabexy(3,1,buffer);
					itoa(ir_winkel-125,buffer,10);
					ausgabexy(3,4,buffer);
					itoa(ir[19],buffer,10);
					ausgabexy(3,10,buffer);
					itoa(ir[14],buffer,10);
					ausgabexy(4,1,buffer);
					itoa(ir[0],buffer,10);
					ausgabexy(4,5,buffer);
					itoa(ir[18],buffer,10);
					ausgabexy(4,10,buffer);
					ausgabe_counter=0;
				}
			}
			ausgabe_counter++;
			for(uint8_t i=0;i<25;i++)	ir[i]=0;
			
*/
