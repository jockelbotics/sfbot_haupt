//Atmega644P - analog.h

#ifndef ANALOG_H
#define ANALOG_H

////////////////////////////////////////////////////////////
//
// Initialisierung der analogen Eing�nge
//
// Befehl: analog_init(); 
//
// Nur ein Beispiel
//
////////////////////////////////////////////////////////////

void analog_init(void) 
{
//    ADCSRA = (1 << ADEN) | (1 << ADPS0) | (1 << ADPS1) | (1 << ADPS2);
  uint16_t result;
 
  // externe Referenzspannung als Refernz f�r den ADC w�hlen:
  // ADMUX = (1<<REFS0);
  ADMUX = 0;
  // Bit ADFR ("free running") in ADCSRA steht beim Einschalten
  // schon auf 0, also single conversion
  ADCSRA  = (1<<ADPS0) | (1<<ADPS1)| (1<<ADPS2);	// Frequenzvorteiler = 128
  ADCSRA |= (1<<ADEN);                  			// ADC aktivieren
 
  /* nach Aktivieren des ADC wird ein "Dummy-Readout" empfohlen, man liest
     also einen Wert und verwirft diesen, um den ADC "warmlaufen zu lassen" */
 
  ADCSRA |= (1<<ADSC);                  // eine ADC-Wandlung 
  while (ADCSRA & (1<<ADSC) ) {         // auf Abschluss der Konvertierung warten
  }
  /* ADCW muss einmal gelesen werden, sonst wird Ergebnis der n�chsten
     Wandlung nicht �bernommen. */
  result = ADCW;
}


////////////////////////////////////////////////////////////
//
// Abfrage der analogen Eing�nge 0-7
//
// Befehl   : y = analog(x); 
// Port     : x = {0;1;2;3;4;5;6;7}
// Ergebniss: Y = {0 - 1024}
//
////////////////////////////////////////////////////////////

uint16_t analog(uint8_t ch) 
{
	// Kanal waehlen, ohne andere Bits zu beeinflu�en
  	ADMUX = (ADMUX & ~(0x1F)) | (ch & 0x1F);
  	ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
    while (ADCSRA & (1<<ADSC));
    return (ADCL | (ADCH << 8));
}


////////////////////////////////////////////////////////////
//
// Abfrage der analogen Eing�nge 0-7 (Liefert den Mittelwert von 5 Messungen)
//
// Befehl   : y = analog5(x); 
// Port     : x = {2;3;4;5;6;7}
// Ergebniss: Y = {0 - 1024}
//
////////////////////////////////////////////////////////////

uint16_t analog5(uint8_t ch) 
{
    uint8_t 	i=0;
	uint32_t wert=0;
	while(i<5)
	{
		i++;
		ADMUX   = ch & 0x1f; 
    	ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
    	while (ADCSRA & (1<<ADSC));
		wert = wert + (ADCL | (ADCH << 8));
	}
    return (wert/i);
}


////////////////////////////////////////////////////////////
//
// Abfrage der analogen Eing�nge 0-7 (Liefert den Mittelwert von 10 Messungen)
//
// Befehl   : y = analog10(x); 
// Port     : x = {0;1;2;3;4;5;6;7}
// Ergebniss: Y = {0 - 1024}
//
////////////////////////////////////////////////////////////

uint16_t analog10(uint8_t ch) 
{
    uint8_t 	i=0;
	uint32_t wert=0;
	while(i<10)
	{
		i++;
		ADMUX   = ch & 0x1f; 
    	ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
    	while (ADCSRA & (1<<ADSC));
		wert = wert + (ADCL | (ADCH << 8));
	}
    return (wert/i);
}

#endif
