//Robo32 - digital.h

#ifndef DIDITAL_H
#define DIGITAL_H

// LEDs einschalten

void led_ein(int nr) // nr = {4;7}
{
    if(nr==4) 
    {
        PORTD |= (1<<nr);
    }
	else if(nr==7)
	{
	    PORTD |= (1<<nr);
	}
	
}

// LEDs ausschalten

void led_aus(int nr) // nr = {4;7}
{
    if(nr==4) 
    {
        PORTD  &= ~(1<<nr); 
    }
	else if(nr==7)
	{
	    PORTD  &= ~(1<<nr);
	}
}

// LEDs tooglen

void led_toggle(int nr) // nr = {4;7}
{
    if(nr==4) 
    {
        PORTD ^= (1<<nr);
    }
	else if(nr==7)
	{
	    PORTD ^= (1<<nr);
	}
}

// Dips auslesen
// Dip0=0bit Dip1=1bit

uint8_t dips_auslesen(void)
{
	uint8_t ergebnis=0;	

	if ((PIND & (1<<2))==0)	ergebnis |= (1<<0);
	if ((PIND & (1<<3))==0)	ergebnis |= (1<<1);

	return ergebnis;
}

// Multiplexer (IC19)
//////////////////////////////
//  Diese Funktion schaltet den Multiplexer in die einzelnen Modi und fragt dann in diesen die Zust�nde der
//  IRs ab und speichert diese Bitweise in ein Byte(returner).
//	Die Speicherung folgt in folgender Reihenfolge
//	Bit0-7: Sensor0,1,2,3,16,18,19 Bit 5:fehlt 
///////////////////////////////

uint8_t multiplexer_ir_abfragen_1()
{
	uint8_t returner=0;

	//Sensor 0
	//			    ABC
	PORTC  &= ~(0b00111000); 					// Modi umschalten
	_delay_us(2);
	if ((PINC & (1<<6))==0)	returner |= (1<<0); // Bei gedr�cktem Taster oder augel�stem IR speichern des Wertes 

	// Analog zu Schritt eins jetzt die folgenden Modi ohne Kommentare

	//Sensor 1
	//			    ABC
	PORTC  &= ~(0b00111000);
	PORTC  |=  (0b00100000); 
	_delay_us(2);
	if ((PINC & (1<<6))==0)	returner |= (1<<1);

	//Sensor 2 
	//			    ABC
	PORTC  &= ~(0b00111000);
	PORTC  |=  (0b00010000); 
	_delay_us(2);
	if ((PINC & (1<<6))==0)	returner |= (1<<2);

	//Sensor 3
	//			    ABC
	PORTC  &= ~(0b00111000);
	PORTC  |=  (0b00110000);   
	_delay_us(2);
	if ((PINC & (1<<6))==0)	returner |= (1<<3);
	
	//Sensor 16
	//			    ABC
	PORTC  &= ~(0b00111000);
	PORTC  |=  (0b00001000); 
	_delay_us(2);
	if ((PINC & (1<<6))==0)	returner |= (1<<4);

	//Sensor 18
	//			    ABC
	PORTC  &= ~(0b00111000);
	PORTC  |=  (0b00011000); 
	_delay_us(2);
	if ((PINC & (1<<6))==0)	returner |= (1<<6);

	//Sensor 19
	//			    ABC
	PORTC  &= ~(0b00111000);
	PORTC  |=  (0b00111000);   
	_delay_us(2);
	if ((PINC & (1<<6))==0)	returner |= (1<<7);

	return returner;
}

// Multiplexer (IC27)
//////////////////////////////
//  Diese Funktion schaltet den Multiplexer in die einzelnen Modi und fragt dann in diesen die Zust�nde der
//  IRs ab und speichert diese Bitweise in ein Byte(returner).
//	Die Speicherung folgt in folgender Reihenfolge
//	Bit0-7: Sensor 6,7,8,9,10,17,23,24 
///////////////////////////////
uint8_t multiplexer_ir_abfragen_2()
{
	uint8_t returner=0;

	//Sensor 23
	//			  CBA  
	PORTA  &= ~(0b11100000); 					// Modi umschalten
	_delay_us(2);
	if ((PINA & (1<<4))==0)	returner |= (1<<6); // Bei gedr�cktem Taster oder augel�stem IR speichern des Wertes 

	// Analog zu Schritt eins jetzt die folgenden Modi ohne Kommentare

	//Sensor 6
	//			 CBA
	PORTA  |= (0b00100000); 
	_delay_us(2);
	if ((PINA & (1<<4))==0)	returner |= (1<<0);

	//Sensor 17
	//			  CBA
	PORTA  &= ~(0b11100000);
	PORTA  |=  (0b01000000); 
	_delay_us(2);
	if ((PINA & (1<<4))==0)	returner |= (1<<5);

	//Sensor 9
	//			  CBA  
	PORTA  |=  (0b00100000);   
	_delay_us(2);
	if ((PINA & (1<<4))==0)	returner |= (1<<3);	

	//Sensor 10
	//			  CBA
	PORTA  &= ~(0b11100000);
	PORTA  |=  (0b10000000); 
	_delay_us(2);
	if ((PINA & (1<<4))==0)	returner |= (1<<4);

	//Sensor 24
	//			 CBA
	PORTA  |= (0b00100000);   
	_delay_us(2);
	if ((PINA & (1<<4))==0)	returner |= (1<<7);

	//Sensor 7
	//			  CBA
	PORTA  &= ~(0b11100000);
	PORTA  |=  (0b11000000); 
	_delay_us(2);
	if ((PINA & (1<<4))==0)	returner |= (1<<1);

	//Sensor 8
	//			  CBA
	PORTA  &= ~(0b11100000);
	PORTA  |=  (0b11100000);   
	_delay_us(2);
	if ((PINA & (1<<4))==0)	returner |= (1<<2);

	return returner;
}

//IR-Fern auslesen
//Anordnung der Fern: 20|12| 11| 5| 15| 4
//Bit 0: Sensor 4	Bit 1: Sensor 5		Bit 2: Sensor 15
//Bit 3: Sensor 11	Bit 4: Sensor 12	Bit 5: Sensor 20
uint8_t ir_abfragen_1(void)
{
	uint8_t returner=0;
	
	//Sensor 4
	if((PINA & (1<<2))==0)	returner |= (1<<0);
	//Sensor 5
	if((PINA & (1<<3))==0)	returner |= (1<<1);
	//Sensor 15
	if((PINA & (1<<1))==0)	returner |= (1<<2);
	//Sensor 11
	if((PINB & (1<<0))==0)	returner |= (1<<3);
	//Sensor 12
	if((PINB & (1<<1))==0)	returner |= (1<<4);
	//Sensor 20
	if((PINB & (1<<2))==0)	returner |= (1<<5);
	return returner;
}

//IR-Nah links in Fahrtrichtung
//Anordnung der 4 Nah-sensoren:
//		Sensor 22
//
//	Sensor 21
//
//	Sensor 13
//
//		Sensor 14
//
//Bit 0: Sensor 22	Bit 1: 21	Bit 2: Sensor 13	Bit 3: Sensor 14
uint8_t ir_abfragen_2(void)
{
	uint8_t returner=0;
	
	//Sensor 22
	if((PINB & (1<<3))==0)	returner |= (1<<0);
	//Sensor 21
	if((PINB & (1<<4))==0)	returner |= (1<<1);
	//Sensor 13
	if((PIND & (1<<5))==0)	returner |= (1<<2);
	//Sensor 14
	if((PIND & (1<<6))==0)	returner |= (1<<3);

	return returner;
}
#endif

