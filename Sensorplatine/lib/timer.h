//Robo32 - timer.h

#ifndef TIMER_H
#define TIMER_H

#define TIM0_PRE 61

void timer_init(void)
{
	TCCR0B |= (1<<CS02) | (1<<CS00);	// Vorteiler f�r 10ms Takt von 1024
	TIMSK0 |= (1<<TOIE0) | (1<<OCIE0A);				// TimerOverflowInterrupt aktivieren
	OCR0A = 81;
	zeit    = 0;
}

// Milisekundenpause

void msleep(int msec)
{
    _delay_ms(msec);
}


// Zeitinterruptroutine

SIGNAL (SIG_OVERFLOW0) 
{
	static uint8_t blinker=0;
	static bool dribb_rdy = false;
	static bool t20ms = false;
	#ifdef HAUPTPLATINE
	// Starten der Kommunikation auf der Hauptplatine
	UDR0 = 255;	
	#endif

	TCNT0  = TIM0_PRE;		//Vorladen f�r 10ms Takt
	zeit++;
	blinker++;

	if(!(zeit%100)) runtime_sec++;

	if(!dribb_rdy && runtime_sec == 3) dribb_rdy = true;

	t20ms ^= 1;		//20ms flag toggle

	if(t20ms) PORTC |= 1<<PC2;	//alle 20ms puls starten
	
	//pulsl�nge setzen
	if(!ui_buffer_recieve[DRIBB] && dribb_rdy) {
		if(dips & 0b10) OCR0A = TIM0_PRE + PULS_AUS;
		else OCR0A = TIM0_PRE + PULS_AUS;
	}
	else if(ui_buffer_recieve[SDRIB] && dribb_rdy) {		//gas
		if(dips & 0b10) OCR0A = 61 + PULS_AN;
		else OCR0A = TIM0_PRE + PULS_AN;
		nah_bereich = false;
	}
		
	else if(nah_bereich && dribb_rdy) {		//anlaufen lassen
		OCR0A = TIM0_PRE + PULS_SL;
	}
	else {
		if(dips & 0b10) OCR0A = TIM0_PRE + PULS_AUS;
		else OCR0A = TIM0_PRE + PULS_AUS;
	}		

	if(blinker==100)
	{
		led_toggle(GRUEN);	//Toggeln der Gr�nen LED in einem 1s Takt
		blinker=0;			
	}
}


SIGNAL (SIG_OUTPUT_COMPARE0A)	//compare interrupt stoppt puls
{
	PORTC &= ~(1<<PC2);
}
#endif
