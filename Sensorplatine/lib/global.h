//ATMega644P - global.h

#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdbool.h>

#ifndef ROBO644_H
#define ROBO644_H

#define SENSORPLATINE

//----------------------------------------------------------
// Definitionen der analogen Ports
//----------------------------------------------------------

// Variablen

// Funktionen

#include "analog.h"

//----------------------------------------------------------
// Definitionen Digitalports
//----------------------------------------------------------

// Variblen


#define ROT 7	// F�r das einschalten der LEDs den Farebennamen benutzen
#define GRUEN 4	// Rot entspricht 7 und Gr�n 4

// Funktionen
#include "digital.h"


// Interne UART - PB
#include "uart_intern.h"
//----------------------------------------------------------
// Definitionen Timer
//----------------------------------------------------------

// Variablen

volatile uint32_t zeit    = 0;     // Tausendstelsekunden
volatile uint32_t zeit_old = 0;
volatile uint32_t runtime_sec = 0; // Sekunden

uint8_t display = 0;

char zeile1[21];
char zeile2[21];
char zeile3[21];
char zeile4[21];

uint8_t ir_hinten[5]= {0};	//links nach rechts (fahrtrichtung)
uint8_t ir_rechts[4]= {0};	//oben nach unten
uint8_t ir_links[4]= {0};
uint8_t ir_vorne_fein[6]= {0};	//in/ bzw hinter dribbler
uint8_t ir_vorne[6]= {0};

uint8_t dips=0;

uint8_t temp_ent;

int16_t ir_winkel=0;
uint8_t ir_abstand=0;
bool	nah_bereich = false;

bool unterspannung = false;

// Funktionen

#include "timer.h"

//----------------------------------------------------------
// Definitionen - Display und Tastatur
//----------------------------------------------------------

#define Dev  0xC4

// Funktionen

#include "display_tastatur.h"

//----------------------------------------------------------
// Definitionen Serielle Daten�bertragung
//----------------------------------------------------------

// Standard UART - Peter Fleury
// Funktionen

#include "uart.h"


/* define CPU frequency in Mhz here if not defined in Makefile */
#ifndef F_CPU
#define F_CPU 20000000	//Oder 8MHz
#endif

/* 9600 baud */
#define UART_BAUD_RATE      9600   




#include "display_ausgabe.h"


#endif

//----------------------------------------------------------
// Initialisierung des robo32-boards
//----------------------------------------------------------

void init(void)
{
	analog_init();

	// Initialisierung der Ein- und Ausg�nge
	DDRA = 0b11100000;
	DDRB = 0b00000000;	//Eing�nge 0 Ausg�nge 1
	DDRC = 0b00111100;
	DDRD = 0b10010010;

	//Schalten von Ausg�ngen und Aktivieren der Pull-Ups
	PORTA =0b00011110;
	PORTB =0b00011111;	// Bei einem Ausgang bedeutet 1 das High setzen des Ausgangs
	PORTC =0b01000000;	// Bei einem Eingang bedeutet 1 das aktivieren der Pull-Ups
	PORTD =0b01101110;
	
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) );
	uart_intern_init();
	#ifdef HAUPTPLATINE 
	bus_buffer_init_haupt();
	#endif// HAUPTPLATINE
	#ifdef SENSORPLATINE 
	bus_buffer_init_sensor();
	#endif// SENSORPLATINE
	#ifdef BODENPLATINE 
	bus_buffer_init_boden();
	#endif// BODENPLATINE

    timer_init();
    sei();
	msleep(100);
/*
	i2c_init();
	if(i2c_start(Dev+I2C_WRITE)) display =0;
	else display = 1;
*/
}

