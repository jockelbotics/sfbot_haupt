#include <stdlib.h>
#include <inttypes.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#ifndef UART_INTERN_H
#define UART_INTERN_H

////////////////////////////////////////////////////////////
// Philipp Baumann - Atmega644P - uart_intern.h
//
// 1. Initialisierung und Implementierung einer einfachen UART-Kommunikation
// 2. Zuordnung von ByteNr. zu Funktion
//
////////////////////////////////////////////////////////////

volatile uint8_t ui_buffer_send[48];
volatile uint8_t ui_buffer_recieve[48];
volatile uint8_t bus_intern_flags; // Flags f�r interne Buskommunikation Beschreibung siehe uart_intern.c

void uart_intern_init(void);

void bus_buffer_init_haupt(void);
void bus_buffer_init_sensor(void);
void bus_buffer_init_boden(void);

/////////////////////////////////////////////////////
//	Definition der Bytes die gesendet und empfangen werden
//	Reihenfolge steht im Wiki http://www.robotik-ag.eu/wiki/index.php?title=BUS_2014
//	freie Pl�tze k�nnen noch beschrieben werden zur Zeit werden Dummies geschickt
/////////////////////////////////////////////////////
#define STARTBYTE			0	
#define INVERTIERTES_BYTE	1
#define V_MOT0				4
#define V_MOT1				5
#define V_MOT2				6
#define V_MOT3				7
#define K_MOT				8
#define DRIBB				9
#define KICK				10
#define SPANNUNG			11
#define BODENSENSOREN		12
#define BALLWINKEL			13
#define BALLENTFERNUNG		14
#define VIMOT0				24
#define SMOT0A				25
#define VIMOT1				26
#define SMOT1A				27
#define VIMOT2				28
#define SMOT2A				29
#define VIMOT3				30
#define SMOT3A				31
#define SDRIB				32
#define SKICK				33


#endif	// endif UART_INTERN_H
