////////////////////////////////////////////////////////////
// Atmega644PA - uart_intern.h
//
// Initialisierung und Implementerirung der internen Kommunikation �ber UART0
//
// Beschreibung:
//	 Initialisierung der Schnittstelle erfolgt �ber die Funktion uart_intern_init().
//	 Die Daten�bertragung l�st einen RX-Interrupt aus (alle Prozessoren lesen ALLES mit).
//	 Die RX-Interrupt-Routine speichert das empf Byte in ui_buffer_recieve und sendet
//	 ein Byte, falls dieser Prozessor das n�chste Byte senden soll.
//   Senden soll dieser Prozessor falls in ui_buffer_send ein Wert >=250 steht.
//	 Das erste Byte sendet immer die Hauptplatine �ber der TOVF0.
////////////////////////////////////////////////////////////

#include "uart_intern.h"

////////////////////////////////////////////////////////////
//
// Initialisierung der internen UART
//
// Befehl: uart_intern_init(); 
//
// Beschreibung:
//	 Initialisiert die UART0 mit:
//		Baudrate 250kHz (bei F_CPU 20MHz)
//		8 Datenbits pro Zeichen, 1 Stopbit, ohne Parit�t, Asynchron
//
////////////////////////////////////////////////////////////
void uart_intern_init(void){

	// Initialisierung des Bus	
	UCSR0A &= 0b11100000;		// Enable nothing, Disable U2X, MPCM(Multi-Processor Com. Mode), Reset errors
	UCSR0B  = 0b10011000;		// Enable RXCI/RX/TX; Disable TXCI/UDREI
	UCSR0C  = 0b00000110;		// Async. UART, no parity, 8 data bits, 1 stop bit
	UBRR0H  = 0;				// Set UBRR = 4 (Baudrate 250kHz for F_CPU = 20MHz)
	UBRR0L  = 4;
	return;
}

void bus_buffer_init_haupt(void)
{
	uint8_t count;
	for(count=0; count<48; count++) ui_buffer_send[count]= 0xFF;
	// Setzen der zu sendenden Bytes f�r die Hauptplatine
	ui_buffer_send[STARTBYTE]= 0xFF;
	ui_buffer_send[1]=0x00;
	ui_buffer_send[2]=0x7F;
	ui_buffer_send[3]=0x7F;
	ui_buffer_send[V_MOT0]=0;
	ui_buffer_send[V_MOT1]=0;
	ui_buffer_send[V_MOT2]=0;
	ui_buffer_send[V_MOT3]=0;
	ui_buffer_send[K_MOT]=100;
	ui_buffer_send[DRIBB]=0x00;
	ui_buffer_send[KICK]=0x00;
	//ui_buffer_send[15]=0x7F;
	//ui_buffer_send[16]=0x7F;
	//ui_buffer_send[17]=0x7F;
	//ui_buffer_send[18]=0x7F;
	//ui_buffer_send[19]=0x7F;
	//ui_buffer_send[20]=0x7F;
	//ui_buffer_send[21]=0x7F;
	//ui_buffer_send[22]=0x7F;
	ui_buffer_send[23]=0xFF;	//BP sensor
	
	//ui_buffer_send[SPANNUNG]=0x7F;
	//ui_buffer_send[BODENSENSOREN]=0x7F;
	//ui_buffer_send[BALLWINKEL]=0x7F;
	//ui_buffer_send[BALLENTFERNUNG]=0x7F;

	//Dummies f�r Leipold Komponenten
	
	// Motor 0
	//ui_buffer_send[VIMOT0]=24;
	//ui_buffer_send[SMOT0A]=25;
	// Motor 1
	//ui_buffer_send[VIMOT1]=26;
	//ui_buffer_send[SMOT1A]=27;
	// Motor 2
	//ui_buffer_send[VIMOT2]=28;
	//ui_buffer_send[SMOT2A]=29;
	// Motor 3
	//ui_buffer_send[VIMOT3]=30;
	//ui_buffer_send[SMOT3A]=31;
	// Dribbler und Kicker
	//ui_buffer_send[SDRIB]=0x7F;
	//ui_buffer_send[SKICK]=0x7F;
	

	ui_buffer_send[34]=0x7F;
	ui_buffer_send[35]=0x7F;
	ui_buffer_send[36]=0x7F;
	ui_buffer_send[37]=0x7F;
	ui_buffer_send[38]=0x00;		//zu kalibrierender bodensensor
	ui_buffer_send[39]=0x00;		//kalibrierungswert
	ui_buffer_send[40]=0xFF;		//BP sensor
	ui_buffer_send[41]=0xFF;		//BP sensor
	ui_buffer_send[42]=0x7F;
	ui_buffer_send[43]=0x7F;
	ui_buffer_send[44]=0x7F;
	ui_buffer_send[45]=0x7F;
	ui_buffer_send[46]=0x7F;
	ui_buffer_send[47]=0x01;
}

void bus_buffer_init_sensor(void)
{
	uint8_t count;
	for(count=0; count<48; count++) ui_buffer_send[count]= 0xFF;
	// Setzen der zu sendenden Bytes f�r die Sensorplatine
	ui_buffer_send[BALLWINKEL]=13;
	ui_buffer_send[BALLENTFERNUNG]=14;
}

void bus_buffer_init_boden(void)
{
	uint8_t count;
	for(count=0; count<48; count++) ui_buffer_send[count]= 0xFF;
	// Setzen der zu sendenden Bytes f�r die Bodenplatine
	ui_buffer_send[SPANNUNG]=1;
	ui_buffer_send[BODENSENSOREN]=12;
	
	ui_buffer_send[15]=0x71;
	ui_buffer_send[16]=0x72;
	ui_buffer_send[17]=0x73;
	ui_buffer_send[18]=0x74;
	ui_buffer_send[19]=0x75;
	ui_buffer_send[20]=0x76;
	ui_buffer_send[21]=0x77;
	ui_buffer_send[22]=0x78;
	ui_buffer_send[23]=0x78;

	ui_buffer_send[40]=0x78;
	ui_buffer_send[41]=0x78;
}
////////////////////////////////////////////////////////////
//
// Implementierung der RX-Interrupt-Routine
//
// Befehl: wird vom System aufgerufen 
//
// Beschreibung:
//	 Pr�ft bei jedem Empfangen-Byte ob die Kommunikation korrekt mit
//	 Startbyte(0xFF) beginnt oder das Datenbyte zu gro� ist.
//	 Liegt kein Fehler vor wird das empf. Byte im Buffer gespeichert,
//	 der Index hochgez�hlt und eventuell ein Byte gesendet.
//
//	 Das Senden eines Byte geschieht immer dann, wenn im Sendebuffer
//	 an der richtigen Stelle ein Wert !=0 steht.
//
// OUT:
//	 bus_intern_flags:  Bit0: Buszyklus vollst�ndig, 
//	 					Rest: Reserviert f�r Fehlerbetrachtung
////////////////////////////////////////////////////////////
SIGNAL(SIG_USART_RECV){
	static uint8_t index = 0;
	static uint8_t blinker=0;
	uint8_t data_tmp = UDR0;

	//PORTD |= (1<<4); 	
	
	//_delay_us(10);

	if((data_tmp > 250) && (index != 0)){PORTD |= (1<<4); ui_buffer_send[47]=index;} //gruen ein bei Wert �ber 250 und nicht erstes Byte
	if(data_tmp > 250)	index = 0;

	if((index == 0) && (data_tmp!=255)) return;
	if((index == 1) && (data_tmp!=(0)))	{ index = 0; return;}
	
	ui_buffer_recieve[index] = data_tmp;
	if((ui_buffer_send[index] >= 250)&& (index != 0)) _delay_us(10);
	index++;
	if(index == 48){ index = 0; bus_intern_flags = 0b1; blinker++; PORTD &= ~(1<<4);}
	if(ui_buffer_send[index] <= 250) UDR0 = ui_buffer_send[index];

//	PORTC ^= (1<<2);
		 	
//	if(blinker==50) {PORTD ^= (1<<4);blinker=0;}
	return;
 }
