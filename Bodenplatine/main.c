// Robotik-AG ATMega644PA - main.c

#include "global.h"
// Hauptprogramm


#define SENSOR_INDEX_MASK (0b00001111)
#define STEUERBITS ((1<<4) | (1<<5))

//// Zu tun:

// analog Messung auf 8 Serienmessungen umstellen
// Schwellwerte im EEPROM einbinden
// Kontrolle das kein Wert gr��er als 250 in den Bus kommt (Spannung, Analogwerte, Bodensensorbyte)
// entr�mpeln: displaysoftware entfernen, uart software entfernen, i2cmaster , Rote LED 5
// Unterscheidung beider Roboter (EEPROM)

int main (void)
{	
	uint16_t 	spannung		=	0;		//aktueller Spannungswert
	uint8_t 	i 				=	0;		//Z�hler
	uint8_t		boden			=	0;		//Speicher f�r Bits der Bodensensoren 
	uint8_t 	schwellwert[11] 	= 	{0};	//Feld f�r Schwellwerte 0-10
	uint8_t 	triggersum[11] 	= 	{0};	//Feld f�r Summen der Werte der Bodensensoren
	uint8_t		sw				=	0;		//Schwellwert
	uint8_t		bp_sensor		=	0;		//Index

   // Initialisierung des Robo-Boards
    init();

	

	//�bernahme der Schwellwerte aus EEPROM beim Einschalten (ungetestet, daher auskommentiert)
	for(i=1;i<11;i++) {
		schwellwert[i] = eeprom_read_byte(((uint8_t*)(i+1)));
	
		if (schwellwert[i] < 0 || schwellwert[i] > 250)
		{
			schwellwert[i] = 155;
			eeprom_write_byte(((uint8_t*)(i+1)), schwellwert[i]);	
		}
	}	



	//festgelegte Schwellwerte der Bodensensoren 0-7
//	{
/*	schwellwert[0] = 155;		
	schwellwert[1] = 155;
	schwellwert[2] = 155;
	schwellwert[3] = 155;
	schwellwert[4] = 155;
	schwellwert[5] = 155;
	schwellwert[6] = 155;
	schwellwert[7] = 155;

*/
//					data fields
//	[[/, PA1, PA2, PA3 ,PA4, PA5, PA6, X0, X1, X2, X3]]
//
//
//					   vorne								vorne
//						PA3									  3
//						PA4									  4
//								
//			X3	X1		PA5		X2	PA2				10  8	  5		9  2 
//						PA6									  6
//
//						X0									  7
//						PA1									  1
	while(1)
	{
		
		// immer lesen
		for(i=1;i<7;i++) if(boden_a(i) < schwellwert[i]) triggersum[i]++;
		
		for(i=0;i<4;i++) if(boden_a_multiplexer(i) < schwellwert[i+7]) triggersum[i+7]++;
		


		//Akkuspannung berechnen
		if(bus_intern_flags == 0b1)
		{

			spannung = akku();						
			if (spannung > 1000) spannung = 1000;
			ui_buffer_send[BP_ANALOG_0] = spannung/4;		//Senden der Akkuspannung (grob)	
			spannung = (10*spannung)/39;
			if (spannung > 250) spannung = 250;	
				
			ui_buffer_send[SPANNUNG]= spannung;				//Senden der Akkuspannung (genau)
		
			// H�lt man Taste 1 gedr�ckt, wird der Schwellwert angezeigt/hochgesendet
			if((ui_buffer_recieve[38] & STEUERBITS) == (1<<4))
			{		
				for(i=1; i<8; i++)
				{	
					ui_buffer_send[BP_ANALOG_0+i] = schwellwert[i];
				}
				
				ui_buffer_send[23] = schwellwert[8];
				ui_buffer_send[40] = schwellwert[9];
				ui_buffer_send[41] = schwellwert[10];
			}
			else
			{
				for(i=1; i<7; i++)
				{

					ui_buffer_send[BP_ANALOG_0+i] = boden_a(i);

				}

				ui_buffer_send[BP_ANALOG_0+7] = boden_a_multiplexer(0);
				ui_buffer_send[23] = boden_a_multiplexer(1);
				ui_buffer_send[40] = boden_a_multiplexer(2);
				ui_buffer_send[41] = boden_a_multiplexer(3);
			}

//////////////////////////////////////////////////////////////////////////////////////////
//			BUS BYTE SORTIERUNG:														//
//				vorne																	//
//				bit 6																	//
//		bit 0	bit 2	bit 4			�u�ere immer zuerst (von links im byte)			//
//				bit 3																	//
//////////////////////////////////////////////////////////////////////////////////////////

			//Zur�cksetzen der Bodensensor�bergabe
			boden=0;

			//Bits werden gesetzt und in boden gespeichert
			if((triggersum[4]  + triggersum[3])>0) boden |= (1 << 6);
			if((triggersum[9]  + triggersum[2])>0) boden |= (1 << 4);		
			if((triggersum[1]  + triggersum[7])>0) boden |= (1 << 3);
			if((triggersum[10] + triggersum[8])>0) boden |= (1 << 0);
			if((triggersum[5]  + triggersum[6])>0) boden |= (1 << 2);

			//boden wird auf 250 begrenzt & das Byte wird gesendet
			if(boden>250) boden=250;
			ui_buffer_send[BODENSENSOREN]= boden;

			//die errechneten Summen der Bodensensoren werden wieder auf 0 gesetzt
			memset(triggersum, 0, sizeof(triggersum));	//reset
		

			// Schwellwert ins EEPROM, wenn 6 Bit gesetzt
			if(ui_buffer_recieve[38] & (1<<5))
			{
				sw = ui_buffer_recieve[39];
				bp_sensor = ui_buffer_recieve[38] & SENSOR_INDEX_MASK; // nach index filtern
				
				// Check, ob Sensorindex als auch Schwellwert valid
				if(bp_sensor > 0 && bp_sensor < 8 && sw > 0 && sw < 250) 
				{
					schwellwert[bp_sensor] = sw;
					eeprom_write_byte(((uint8_t*)(bp_sensor+1)), sw);
										
				}
			}
	//		else if


/*			if(ui_buffer_recieve[38] > 0 && ui_buffer_recieve[38] < 8 && !bus_intern_flags) {		//incoming cal
				schwellwert[ui_buffer_recieve[38]] = ui_buffer_recieve[39];
				eeprom_write_byte(((uint8_t*)ui_buffer_recieve[38]+1), ui_buffer_recieve[39]);
				ui_buffer_send[38] = 0;
			}
*/

			bus_intern_flags=0;
		}
	}

    // Ende eigener Code
    while(1);

    return 1;
} 
