//Atmega644P - analog.h

#ifndef ANALOG_H
#define ANALOG_H

////////////////////////////////////////////////////////////
//
// Initialisierung der analogen Eing�nge
//
// Befehl: analog_init(); 
//
// Nur ein Beispiel
//
////////////////////////////////////////////////////////////

void analog_init(void) 
{
//    ADCSRA = (1 << ADEN) | (1 << ADPS0) | (1 << ADPS1) | (1 << ADPS2);
  uint16_t result;
 
  // interne Referenzspannung als Refernz f�r den ADC w�hlen:
  ADMUX  = (1<<REFS0);
  //ADMUX = 0;
  // Bit ADFR ("free running") in ADCSRA steht beim Einschalten
  // schon auf 0, also single conversion
  ADCSRA  = (1<<ADPS0) | (1<<ADPS1)| (1<<ADPS2);	// Frequenzvorteiler = 128
  ADCSRA |= (1<<ADEN);                  			// ADC aktivieren
 
  /* nach Aktivieren des ADC wird ein "Dummy-Readout" empfohlen, man liest
     also einen Wert und verwirft diesen, um den ADC "warmlaufen zu lassen" */
 
  ADCSRA |= (1<<ADSC);                  // eine ADC-Wandlung 
  while (ADCSRA & (1<<ADSC) ) {         // auf Abschluss der Konvertierung warten
  }
  /* ADCW muss einmal gelesen werden, sonst wird Ergebnis der n�chsten
     Wandlung nicht �bernommen. */
  result = ADCW;
}


////////////////////////////////////////////////////////////
//
// Abfrage der analogen Eing�nge 0-7
//
// Befehl   : y = analog(x); 
// Port     : x = {0;1;2;3;4;5;6;7}
// Ergebniss: Y = {0 - 1024}
//
////////////////////////////////////////////////////////////


uint8_t set_multiplexer(uint8_t channel) {		//missing logic to save cycles
//	   ABC
//	0b01110000

	PORTC  &= ~(0b01110000);
	_delay_us(2);
	switch(channel) {
		case 0: 			return 0;  break;
		case 1: PORTC  |= (0b01000000);break;
		case 2: PORTC  |= (0b00100000);break;
		case 3: PORTC  |= (0b01100000);break;
		case 4: PORTC  |= (0b00010000);break;
		case 5: PORTC  |= (0b01010000);break;
		case 6: PORTC  |= (0b00110000);break;
		case 7: PORTC  |= (0b01110000);break;
		default: 			return 1;  break;
	}
	return 0;
}


uint16_t analog(uint8_t ch) 
{
	// Kanal waehlen, ohne andere Bits zu beeinflu�en
  	ADMUX = (ADMUX & ~(0x1F)) | (ch & 0x1F);
  	ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
    while (ADCSRA & (1<<ADSC));
    return (ADCL | (ADCH << 8));
}



////////////////////////////////////////////////////////////
//
// Abfrage der analogen Eing�nge 0-7 (Liefert den Mittelwert von 5 Messungen)
//
// Befehl   : y = analog5(x); 
// Port     : x = {2;3;4;5;6;7}
// Ergebniss: Y = {0 - 1024}
//
////////////////////////////////////////////////////////////

uint16_t analog5(uint8_t ch) 
{
    uint8_t 	i=0;
	uint32_t wert=0;
	while(i<5)
	{
		i++;
		ADMUX = (ADMUX & ~(0x1F)) | (ch & 0x1F); 
    	ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
    	while (ADCSRA & (1<<ADSC));
		wert = wert + (ADCL | (ADCH << 8));
	}
    return (wert/i);
}


uint16_t analog8(uint8_t ch) 
{
    uint8_t 	i=0;
	uint32_t wert=0;
	while(i<8)
	{
		i++;
		ADMUX |= ch & 0x1f;
		ADMUX |= (1<<5); 
    	ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
    	while (ADCSRA & (1<<ADSC));
		wert = wert + (ADCH);
	}
    return ((wert)>>1);
}

////////////////////////////////////////////////////////////
//
// Abfrage der analogen Eing�nge 0-7 (Liefert den Mittelwert von 10 Messungen)
//
// Befehl   : y = analog10(x); 
// Port     : x = {0;1;2;3;4;5;6;7}
// Ergebniss: Y = {0 - 1024}
//
////////////////////////////////////////////////////////////

uint16_t analog10(uint8_t ch) 
{
    uint8_t 	i=0;
	uint32_t wert=0;
	while(i<10)
	{
		i++;
		ADMUX = (ADMUX & ~(0x1F)) | (ch & 0x1F); 
    	ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
    	while (ADCSRA & (1<<ADSC));
		wert = wert + (ADCL | (ADCH << 8));
	}
    return (wert/i);
}


uint8_t boden_a(uint8_t ch) 
{
    uint8_t 	i=0;
	uint32_t wert=0;
	while(i<10)
	{
		i++;
		ADMUX = (ADMUX & ~(0x1F)) | (ch & 0x1F); 
    	ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
    	while (ADCSRA & (1<<ADSC));
		wert = wert + (ADCL | (ADCH << 8));
	}
    wert=wert/50;
	if(wert > 250) wert = 250;  
	return wert;
}

uint8_t boden_a_multiplexer(uint8_t ch)
{
	uint8_t 	i=0;
	uint32_t wert=0;
/*
	PORTC  &= ~(0b01110000);
	_delay_us(2);
	PORTC  |= (0b00010000);
	_delay_us(2);	
*/
	set_multiplexer(ch);
	_delay_us(2);
	
	while(i<10)
	{
		i++;
		ADMUX = (ADMUX & ~(0x1F)) | (7 & 0x1F);			//hardcoded channel / multiplexer data pin
		ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
		while (ADCSRA & (1<<ADSC));
		wert = wert + (ADCL | (ADCH << 8));
	}

	wert=wert/50;
	if(wert > 250) wert = 250;
	return wert;
}
/*
uint8_t boden_a(uint8_t ch)
{
    uint8_t      i=0;
     uint32_t wert=0;
     while(i<4)
     {
          i++;
         ADMUX = (ADMUX & ~(0x1F)) | (ch & 0x1F);
		 ADMUX |= (1<<5);
         ADCSRA  = ADCSRA | (1 << ADSC) | (1 << ADIF);
         while (ADCSRA & (1<<ADSC));
          wert = wert + ADCH;
     }
    wert=wert>>2;
     if(wert > 250) wert = 250; 
     return wert;
}
*/
uint16_t akku(void)
{
	uint16_t spannung=0;
	spannung=analog10(0);
	
	return spannung;
}

#endif
