//ATMega644P - global.h

#include <stdlib.h>
#include <inttypes.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>		//eeprom functions
#include <string.h> /* memset */
#include <stdbool.h>

#ifndef ROBO644_H
#define ROBO644_H

#define BODENPLATINE

//----------------------------------------------------------
// Definitionen der analogen Ports
//----------------------------------------------------------

// Variablen

// Funktionen
extern void* memset();

#include "analog.h"

//----------------------------------------------------------
// Definitionen Digitalports
//----------------------------------------------------------

// Variblen

#define ROT 5		// F�r das einschalten der LEDs den Farebennamen benutzen
#define GRUEN 4		// Rot entspricht 5 und Gr�n 4

// Funktionen
#include "digital.h"


//----------------------------------------------------------
// Definitionen Timer
//----------------------------------------------------------

// Variablen

volatile uint32_t zeit    = 0;     // Tausendstelsekunden
volatile uint32_t blinken = 0;     // Hilfsz�hler zum Blinken

// Funktionen

#include "timer.h"

//----------------------------------------------------------
// Definitionen - Display und Tastatur
//----------------------------------------------------------

#define Dev  0xC4

// Funktionen

#include "display_tastatur.h"

//----------------------------------------------------------
// Definitionen Serielle Daten�bertragung
//----------------------------------------------------------

// Standard UART - Peter Fleury
// Funktionen

#include "uart.h"


/* define CPU frequency in Mhz here if not defined in Makefile */
#ifndef F_CPU
#define F_CPU 20000000	//20MHz
#endif

/* 9600 baud */
#define UART_BAUD_RATE      9600   

// Interne UART - PB
#include "uart_intern.h"

#endif

//----------------------------------------------------------
// Initialisierung des robo32-boards
//----------------------------------------------------------

void init(void)
{
	analog_init();

	// Initialisierung der Ein- und Ausg�nge
	DDRA = 0b00000000;
	DDRB = 0b00010000;	//Eing�nge 0 Ausg�nge 1
	DDRC = 0b01110000;
	DDRD = 0b01010010;

	//Schalten von Ausg�ngen und Aktivieren der Pull-Ups
	PORTA =0b00000000;
	PORTB =0b00000000;	// Bei einem Ausgang bedeutet 1 das High setzen des Ausgangs
	PORTC =0b00000000;	// Bei einem Eingang bedeutet 1 das aktivieren der Pull-Ups
	PORTD =0b00000010;
	
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) );
	uart_intern_init();
	#ifdef HAUPTPLATINE 
	bus_buffer_init_haupt();
	#endif// HAUPTPLATINE
	#ifdef SENSORPLATINE 
	bus_buffer_init_sensor();
	#endif// SENSORPLATINE
	#ifdef BODENPLATINE  
	bus_buffer_init_boden();
	#endif// BODENPLATINE

    timer_init();
    sei();
	msleep(10);
}

