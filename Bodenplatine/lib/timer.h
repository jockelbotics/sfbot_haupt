//Robo32 - timer.h

#ifndef TIMER_H
#define TIMER_H

void timer_init(void)
{
	TCCR0B |= (1<<CS02) | (1<<CS00);	// Vorteiler f�r 10ms Takt von 1024
	TIMSK0 |= (1<<TOIE0);				// TimerOverflowInterrupt aktivieren
	zeit    = 0;
	blinken = 100;
}

// Milisekundenpause

void msleep(int msec)
{
    _delay_ms(msec);
}


// Zeitinterruptroutine

SIGNAL (SIG_OVERFLOW0) 
{
	static uint8_t blinker=0;
#ifdef HAUPTPLATINE
	// Starten der Kommunikation auf der Hauptplatine
	UDR0 = 255;	
#endif

	TCNT0  = 61;		//Vorladen f�r 10ms Takt
	zeit++;
	blinker++;
	if(blinker==100)
	{
		led_toggle(GRUEN);	//Toggeln der Gr�nen LED in einem 1s Takt
		blinker=0;			
	}
}

#endif
