//Robo32 - digital.h

#ifndef DIDITAL_H
#define DIGITAL_H

// LEDs einschalten

void led_ein(int nr) // nr = {4;5}
{
    if(nr==4||nr==5) 
    {
        PORTD |= (1<<nr);
    }
}

// LEDs ausschalten

void led_aus(int nr) // nr = {4;5}
{
    if(nr==4||nr==5) 
    {
        PORTD  &= ~(1<<nr); 
    }
}

// LEDs tooglen

void led_toggle(int nr) // nr = {4;5}
{
    if(nr==4||nr==5) 
    {
        PORTD ^= (1<<nr);
    }
}


#endif

